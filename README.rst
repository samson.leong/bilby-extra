=========
Bilby-extra
=========

This is the Bilby Proca strain package. It is used to
extend the Bilby source models, allowing Bilby and parallel-Bilby to 
work with the proca star waveform which takes data files as inputs.

This repo also contains some utility functions to handle with Proca related things.
Including the some functions to convert between Newmann-Penrose scalar and strains.

Future plans
==============
I don't know where would be a good place for these, so I am putting them here :)  

* Divide the current ``Qscan.py`` into a generic (utils) part and a specific (running) part
* Make a separte ``qscan_utils.py`` target at dealing with QSpectrogram stuff
* Move ``proca_wfm`` and ``bbh_wfm`` out of ``source.py`` to ``qscan_utils.py``
* Finish constructing a ``proca_result`` class, just like ``bilby.result`` 
* Maybe refactor the repo a bit?
