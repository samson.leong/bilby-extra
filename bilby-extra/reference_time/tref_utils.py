import numpy as np
from numpy import vectorize

from lal import MTSUN_SI, PI
from lalsimulation import \
    SimInspiralPrecessingNRSur_get_omega_ref, \
    SimInspiralTransformPrecessingNewInitialConditions, \
    GetApproximantFromString

from bilby.gw.conversion import transform_precessing_spins

from functools import lru_cache

@lru_cache(maxsize=10)
def lalsim_GetApproximantFromString(waveform_approximant):
    if isinstance(waveform_approximant, str):
        return GetApproximantFromString(waveform_approximant)
    else:
        raise ValueError("waveform_approximant must be of type str")

nrsur = lalsim_GetApproximantFromString('NRSur7dq4')

''' SimInspiralPrecessingNRSur_get_omega_ref
    (REAL8 t_ref, REAL8 q,
     REAL8 chiAx, REAL8 chiAy, REAL8 chiAz,
     REAL8 chiBx, REAL8 chiBy, REAL8 chiBz,
     REAL8 init_quat0, REAL8 init_quat1, REAL8 init_quat2, REAL8 init_quat3, i
     REAL8 init_orbphase, Approximant approximant)
    -> REAL8
'''
get_omega_ref_from_array = vectorize(SimInspiralPrecessingNRSur_get_omega_ref,
        excluded={'init_quat0', 'init_quat1', 'init_quat2', 'init_quat3', 'init_orbphase', 'approximant'})


def update_poste_ref_freq(posterior):
    np_args = [ posterior[key].astype(float) for key in ('reference_time', 'mass_ratio',
        'spin_1x', 'spin_1y', 'spin_1z', 'spin_2x', 'spin_2y', 'spin_2z') ]
    posterior['reference_frequency'] = get_omega_ref_from_array(*np_args, 1,0,0,0, 0, nrsur) /\
                                    (PI * posterior['total_mass'].astype(float) * MTSUN_SI)
    return posterior


vectorised_SimInspiralPrecessingNRSur_get_omega_ref = \
    np.vectorize(SimInspiralPrecessingNRSur_get_omega_ref,
                 excluded=['init_quat0', 'init_quat1', 'init_quat2', 'init_quat3', 'init_orbphase', 'approximant'])

vectorised_SimInspiralTransformPrecessingNewInitialConditions = \
    np.vectorize(SimInspiralTransformPrecessingNewInitialConditions)


def get_fref_from_tref(reference_time:float, mass_1:float, mass_2:float, theta_jn:float,
                       phase:float, a_1:float, a_2:float, tilt_1:float, tilt_2:float,
                       phi_12:float, phi_jl:float, approximant='NRSur7dq4',
                       iterations:int=1, return_spins:bool=False):
    """ Compute the reference frequency from a reference (geometric) time

    One can verify the result from this function by feeding the output f_ref to
        lalsimulation.SimInspiralPrecessingNRSur_get_t_ref
    with the returned spins, and compare the t_ref with the desired value.

    Parameters
    ==========
    reference_time: The time (in geometric unit) we wish the reference frame to be.
    mass_1:         The mass of the heavier object in solar masses
    mass_2:         The mass of the lighter object in solar masses
    a_1:            Dimensionless primary spin magnitude
    tilt_1:         Primary tilt angle
    phi_12:         Azimuthal angle between the component spins
    a_2:            Dimensionless secondary spin magnitude
    tilt_2:         Secondary tilt angle
    phi_jl:         Azimuthal angle between the total and orbital angular momenta
    theta_jn:       Orbital inclination
    phase:          The phase at coalescence
    approximant:    The approximant of which the surrogate data to use,
                    either NRSur7dq4 or one of NRSurrogate family
    iterations:     Number of iterations to undergo before returning f_ref.
                    Typically, one iteration can already provide sufficient convergence.
    return_spins:   Whether to return the spin components from the last iteration.
    """


    if approximant != 'NRSur7dq4':
        approx = lalsim_GetApproximantFromString(approximant)
    else:
        approx = 93 ## NRSur7dq4

    Mtot = mass_1 + mass_2
    Q = mass_1 / mass_2

    # First construct a guess spin components
    a1t = a_1 * np.sin(tilt_1)
    a2t = a_2 * np.sin(tilt_2)
    s1x = a1t * np.cos(phase)
    s1y = a1t * np.sin(phase)
    s1z = a_1 * np.cos(tilt_1)
    s2x = a2t * np.cos(phi_12 + phase)
    s2y = a2t * np.sin(phi_12 + phase)
    s2z = a_2 * np.cos(tilt_2)

    # Then get a rough approximate of reference frequency
    omega = vectorised_SimInspiralPrecessingNRSur_get_omega_ref(
        reference_time, Q, s1x, s1y, s1z, s2x, s2y, s2z, 1,0,0,0, 0, approx)
    f_ref = omega / np.pi / (Mtot * MTSUN_SI)

    # Iterate to get better spin components and hence reference frequency
    for _ in range(iterations):
        # NOTE - 11/12/23:
        # Yes, the masses input of this function should have been multiplied by
        # MSUN_SI, but apparently, without the scaling works better.
        # Suspect: the orbital L is now much smaller.
        _, s1x, s1y, s1z, s2x, s2y, s2z = transform_precessing_spins(
            theta_jn, phi_jl, tilt_1, tilt_2, phi_12, a_1, a_2, mass_1, mass_2, f_ref, phase
        )
        omega = vectorised_SimInspiralPrecessingNRSur_get_omega_ref(
            reference_time, Q, s1x, s1y, s1z, s2x, s2y, s2z, 1,0,0,0, 0, approx)
        f_ref = omega / np.pi / (Mtot * MTSUN_SI)

    if return_spins:
        return f_ref, (s1x, s1y, s1z, s2x, s2y, s2z)

    return f_ref


def get_fref_from_tref_component_spins(reference_time:float, mass_1:float, mass_2:float,
                                       spin_1x:float, spin_1y:float, spin_1z:float,
                                       spin_2x:float, spin_2y:float, spin_2z:float, approximant='NRSur7dq4',
                                       iterations:int=1, return_spins:bool=False):

    if approximant != 'NRSur7dq4':
        approx = lalsim_GetApproximantFromString(approximant)
    else:
        approx = 93 ## NRSur7dq4

    Mtot = mass_1 + mass_2
    Q = mass_1 / mass_2

    # Then get a rough approximate of reference frequency
    omega = vectorised_SimInspiralPrecessingNRSur_get_omega_ref(
        reference_time, Q, spin_1x, spin_1y, spin_1z,
        spin_2x, spin_2y, spin_2z, 1,0,0,0, 0, approx)
    f_ref = omega / np.pi / (Mtot * MTSUN_SI)

    return f_ref
