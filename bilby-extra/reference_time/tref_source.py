import numpy as np

from bilby.gw.source import _base_lal_cbc_fd_waveform

import lal
from lal import PC_SI, MSUN_SI
import lalsimulation as lalsim

from .tref_utils import lalsim_GetApproximantFromString

from ..utils import logger


# TODO: Move this to somewhere else (others?)
def lal_component_spins_binary_black_hole(
        frequency_array, mass_1, mass_2, luminosity_distance, iota, phase,
        spin_1x=0.0, spin_1y=0.0, spin_1z=0.0, spin_2x=0.0, spin_2y=0.0, spin_2z=0.0,
        lambda_1=0.0, lambda_2=0.0, eccentricity=0.0, **waveform_kwargs):
    """ Generate a cbc waveform model using lalsimulation

    Parameters
    ==========
    frequency_array: array_like
        The frequencies at which we want to calculate the strain
    mass_1: float
        The mass of the heavier object in solar masses
    mass_2: float
        The mass of the lighter object in solar masses
    luminosity_distance: float
        The luminosity distance in megaparsec
    a_1: float
        Dimensionless primary spin magnitude
    tilt_1: float
        Primary tilt angle
    phi_12: float
        Azimuthal angle between the component spins
    a_2: float
        Dimensionless secondary spin magnitude
    tilt_2: float
        Secondary tilt angle
    phi_jl: float
        Azimuthal angle between the total and orbital angular momenta
    theta_jn: float
        Orbital inclination
    phase: float
        The phase at reference frequency or peak amplitude (depends on waveform)
    eccentricity: float
        Binary eccentricity
    lambda_1: float
        Tidal deformability of the more massive object
    lambda_2: float
        Tidal deformability of the less massive object
    kwargs: dict
        Optional keyword arguments

    Returns
    =======
    dict: A dictionary with the plus and cross polarisation strain modes
    """
    waveform_approximant = waveform_kwargs['waveform_approximant']
    reference_frequency = waveform_kwargs['reference_frequency']
    minimum_frequency = waveform_kwargs['minimum_frequency']
    maximum_frequency = waveform_kwargs['maximum_frequency']
    catch_waveform_errors = waveform_kwargs['catch_waveform_errors']
    pn_spin_order = waveform_kwargs['pn_spin_order']
    pn_tidal_order = waveform_kwargs['pn_tidal_order']
    pn_phase_order = waveform_kwargs['pn_phase_order']
    pn_amplitude_order = waveform_kwargs['pn_amplitude_order']
    waveform_dictionary = waveform_kwargs.get(
        'lal_waveform_dictionary', lal.CreateDict()
    )

    approximant = lalsim_GetApproximantFromString(waveform_approximant)

    if pn_amplitude_order != 0:
        start_frequency = lalsim.SimInspiralfLow2fStart(
            float(minimum_frequency), int(pn_amplitude_order), approximant
        )
    else:
        start_frequency = minimum_frequency

    delta_frequency = frequency_array[1] - frequency_array[0]

    frequency_bounds = ((frequency_array >= minimum_frequency) *
                        (frequency_array <= maximum_frequency))

    luminosity_distance = luminosity_distance * 1e6 * PC_SI
    mass_1 = mass_1 * MSUN_SI
    mass_2 = mass_2 * MSUN_SI

    longitude_ascending_nodes = 0.0
    mean_per_ano = 0.0

    lalsim.SimInspiralWaveformParamsInsertPNSpinOrder(
        waveform_dictionary, int(pn_spin_order))
    lalsim.SimInspiralWaveformParamsInsertPNTidalOrder(
        waveform_dictionary, int(pn_tidal_order))
    lalsim.SimInspiralWaveformParamsInsertPNPhaseOrder(
        waveform_dictionary, int(pn_phase_order))
    lalsim.SimInspiralWaveformParamsInsertPNAmplitudeOrder(
        waveform_dictionary, int(pn_amplitude_order))
    lalsim.SimInspiralWaveformParamsInsertTidalLambda1(
        waveform_dictionary, float(lambda_1))
    lalsim.SimInspiralWaveformParamsInsertTidalLambda2(
        waveform_dictionary, float(lambda_2))

    for key, value in waveform_kwargs.items():
        func = getattr(lalsim, "SimInspiralWaveformParamsInsert" + key, None)
        if func is not None:
            func(waveform_dictionary, value)

    if waveform_kwargs.get('numerical_relativity_file', None) is not None:
        lalsim.SimInspiralWaveformParamsInsertNumRelData(
            waveform_dictionary, waveform_kwargs['numerical_relativity_file'])

    if ('mode_array' in waveform_kwargs) and waveform_kwargs['mode_array'] is not None:
        mode_array = waveform_kwargs['mode_array']
        mode_array_lal = lalsim.SimInspiralCreateModeArray()
        for mode in mode_array:
            lalsim.SimInspiralModeArrayActivateMode(mode_array_lal, mode[0], mode[1])
        lalsim.SimInspiralWaveformParamsInsertModeArray(waveform_dictionary, mode_array_lal)

    if lalsim.SimInspiralImplementedFDApproximants(approximant):
        wf_func = lalsim.SimInspiralChooseFDWaveform
    else:
        wf_func = lalsim.SimInspiralFD
    try:
        hplus, hcross = wf_func(
            mass_1, mass_2, spin_1x, spin_1y, spin_1z, spin_2x, spin_2y,
            spin_2z, luminosity_distance, iota, phase,
            longitude_ascending_nodes, eccentricity, mean_per_ano, delta_frequency,
            start_frequency, maximum_frequency, reference_frequency,
            waveform_dictionary, approximant)
    except Exception as e:
        if not catch_waveform_errors:
            raise
        else:
            EDOM = (e.args[0] == 'Internal function call failed: Input domain error')
            if EDOM:
                failed_parameters = dict(mass_1=mass_1, mass_2=mass_2,
                                         spin_1=(spin_1x, spin_2y, spin_1z),
                                         spin_2=(spin_2x, spin_2y, spin_2z),
                                         luminosity_distance=luminosity_distance,
                                         iota=iota, phase=phase,
                                         eccentricity=eccentricity,
                                         start_frequency=start_frequency)
                # logger.warning("Evaluating the waveform failed with error: {}\n".format(e) +
                #                "The parameters were {}\n".format(failed_parameters) +
                #                "Likelihood will be set to -inf.")
                return None
            else:
                raise

    h_plus = np.zeros_like(frequency_array, dtype=complex)
    h_cross = np.zeros_like(frequency_array, dtype=complex)

    if len(hplus.data.data) > len(frequency_array):
        # logger.debug("LALsim waveform longer than bilby's `frequency_array`" +
        #              "({} vs {}), ".format(len(hplus.data.data), len(frequency_array)) +
        #              "probably because padded with zeros up to the next power of two length." +
        #              " Truncating lalsim array.")
        h_plus = hplus.data.data[:len(h_plus)]
        h_cross = hcross.data.data[:len(h_cross)]
    else:
        h_plus[:len(hplus.data.data)] = hplus.data.data
        h_cross[:len(hcross.data.data)] = hcross.data.data

    h_plus *= frequency_bounds
    h_cross *= frequency_bounds

    if wf_func == lalsim.SimInspiralFD:
        dt = 1 / hplus.deltaF + (hplus.epoch.gpsSeconds + hplus.epoch.gpsNanoSeconds * 1e-9)
        time_shift = np.exp(-1j * 2 * np.pi * dt * frequency_array[frequency_bounds])
        h_plus[frequency_bounds] *= time_shift
        h_cross[frequency_bounds] *= time_shift

    return dict(plus=h_plus, cross=h_cross)


try:
    from .tref_utils import get_fref_from_tref, \
        get_fref_from_tref_component_spins
except ImportError:
    logger.warning('Cannot import reference time related functions, quitting.')
    quit()


def lal_NRSur_tref_binary_black_hole(
        frequency_array, mass_1, mass_2, luminosity_distance, a_1, tilt_1,
        phi_12, a_2, tilt_2, phi_jl, theta_jn, phase, **kwargs):
    """ A Binary Black Hole waveform model using lalsimulation

    ** This model does not accept reference_frequency as argument **
    ** It is also intended to serve the NRSurrogate approximants only **

    Parameters
    ==========
    frequency_array: array_like
        The frequencies at which we want to calculate the strain
    mass_1: float
        The mass of the heavier object in solar masses
    mass_2: float
        The mass of the lighter object in solar masses
    luminosity_distance: float
        The luminosity distance in megaparsec
    a_1: float
        Dimensionless primary spin magnitude
    tilt_1: float
        Primary tilt angle
    phi_12: float
        Azimuthal angle between the two component spins
    a_2: float
        Dimensionless secondary spin magnitude
    tilt_2: float
        Secondary tilt angle
    phi_jl: float
        Azimuthal angle between the total binary angular momentum and the
        orbital angular momentum
    theta_jn: float
        Angle between the total binary angular momentum and the line of sight
    phase: float
        The phase at coalescence
    kwargs: dict
        Optional keyword arguments
        Supported arguments:

        - waveform_approximant
        - reference_time
          In geometric unit, and is expected to be negative. Default: -100.
          It will be converted to the f_ref according to the input parameters,
          then feed into LALInspiral via the reference_frequency option.
        - minimum_frequency
        - maximum_frequency
        - catch_waveform_errors
        - pn_spin_order
        - pn_tidal_order
        - pn_phase_order
        - pn_amplitude_order
        - mode_array:
          Activate a specific mode array and evaluate the model using those
          modes only.  e.g. waveform_arguments =
          dict(waveform_approximant='IMRPhenomHM', mode_array=[[2,2],[2,-2])
          returns the 22 and 2-2 modes only of IMRPhenomHM.  You can only
          specify modes that are included in that particular model.  e.g.
          waveform_arguments = dict(waveform_approximant='IMRPhenomHM',
          mode_array=[[2,2],[2,-2],[5,5],[5,-5]]) is not allowed because the
          55 modes are not included in this model.  Be aware that some models
          only take positive modes and return the positive and the negative
          mode together, while others need to call both.  e.g.
          waveform_arguments = dict(waveform_approximant='IMRPhenomHM',
          mode_array=[[2,2],[4,-4]]) returns the 22 and 2-2 of IMRPhenomHM.
          However, waveform_arguments =
          dict(waveform_approximant='IMRPhenomXHM', mode_array=[[2,2],[4,-4]])
          returns the 22 and 4-4 of IMRPhenomXHM.
        - lal_waveform_dictionary:
          A dictionary (lal.Dict) of arguments passed to the lalsimulation
          waveform generator. The arguments are specific to the waveform used.

    Returns
    =======
    dict: A dictionary with the plus and cross polarisation strain modes
    """
    waveform_kwargs = dict(
        waveform_approximant='NRSur7dq4', reference_time=-100,
        minimum_frequency=20.0, maximum_frequency=frequency_array[-1],
        catch_waveform_errors=False, pn_spin_order=-1, pn_tidal_order=-1,
        pn_phase_order=-1, pn_amplitude_order=0)

    input_fref = kwargs.pop('reference_frequency', None)
    if input_fref is not None:
        err_msg = 'This model does not accept reference_frequency as input, removing it.'
        pass

    waveform_kwargs.update(kwargs)
    if waveform_kwargs['waveform_approximant'] not in ['NRSur4dq2', 'NRSur7dq4']:
        err_msg = 'The reference frequency conversion function is not ' + \
                'for approximant outside the NRSurrogate family.'
        raise ValueError(err_msg)

    t_ref = waveform_kwargs.pop('reference_time')
    approx = waveform_kwargs['waveform_approximant']

    f_ref = get_fref_from_tref(t_ref, mass_1, mass_2, theta_jn, phase,
                               a_1, a_2, tilt_1, tilt_2, phi_12, phi_jl,
                               approximant=approx)

    waveform_kwargs['reference_frequency'] = f_ref

    return _base_lal_cbc_fd_waveform(
        frequency_array=frequency_array, mass_1=mass_1, mass_2=mass_2,
        luminosity_distance=luminosity_distance, theta_jn=theta_jn, phase=phase,
        a_1=a_1, a_2=a_2, tilt_1=tilt_1, tilt_2=tilt_2, phi_12=phi_12,
        phi_jl=phi_jl, **waveform_kwargs)


def lal_tref_binary_black_hole_component_spins(
        frequency_array, mass_1, mass_2, luminosity_distance, iota, phase,
        spin_1x=0.0, spin_1y=0.0, spin_1z=0.0, spin_2x=0.0, spin_2y=0.0, spin_2z=0.0,
        **kwargs):

    waveform_kwargs = dict(
        waveform_approximant='NRSur7dq4', reference_time=-100,
        minimum_frequency=20.0, maximum_frequency=frequency_array[-1],
        catch_waveform_errors=False, pn_spin_order=-1, pn_tidal_order=-1,
        pn_phase_order=-1, pn_amplitude_order=0)

    input_fref = kwargs.pop('reference_frequency', None)
    if input_fref is not None:
        err_msg = 'This model does not accept reference_frequency as input, removing it.'
        pass

    waveform_kwargs.update(kwargs)
    if waveform_kwargs['waveform_approximant'] not in ['NRSur4dq2', 'NRSur7dq4']:
        err_msg = 'The reference frequency conversion function is not ' + \
                'for approximant outside the NRSurrogate family.'
        raise ValueError(err_msg)

    t_ref = waveform_kwargs.pop('reference_time')
    approx = waveform_kwargs['waveform_approximant']

    f_ref = get_fref_from_tref_component_spins(t_ref, mass_1, mass_2,
                                               spin_1x, spin_1y, spin_1z,
                                               spin_2x, spin_2y, spin_2z,
                                               approximant=approx)

    waveform_kwargs['reference_frequency'] = f_ref

    return lal_component_spins_binary_black_hole(
        frequency_array=frequency_array, mass_1=mass_1, mass_2=mass_2,
        luminosity_distance=luminosity_distance, iota=iota, phase=phase,
        spin_1x=spin_1x, spin_1y=spin_1y, spin_1z=spin_1z,
        spin_2x=spin_2x, spin_2y=spin_2y, spin_2z=spin_2z,
        **waveform_kwargs)
