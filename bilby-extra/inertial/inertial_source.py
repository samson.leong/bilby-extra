from bilby.gw.source import _base_lal_cbc_fd_waveform

import lal
import lalsimulation as lalsim

from ..utils import logger


def lal_inertial_binary_black_hole(
        frequency_array, mass_1, mass_2, luminosity_distance, theta_jn, phase,
        a_1=0.0, a_2=0.0, tilt_1=0.0, tilt_2=0.0, phi_12=0.0, phi_jl=0.0,
        **kwargs):

    waveform_dictionary = kwargs.get(
        'lal_waveform_dictionary', lal.CreateDict()
    )
    inertial_mode_array = kwargs.get(
            'inertial_mode_array', None
    )
    if inertial_mode_array is None:
        logger.warning('Inertial modes not specified, considering using the regular `lal_binary_black_hole` function instead.')
    else:
        inertial_mode_array_lal = lalsim.SimInspiralCreateModeArray()
        for mode in inertial_mode_array:
            lalsim.SimInspiralModeArrayActivateMode(inertial_mode_array_lal, mode[0], mode[1])
        lalsim.SimInspiralWaveformParamsInsertInertialModeArray(waveform_dictionary, inertial_mode_array_lal)

    kwargs['lal_waveform_dictionary'] = waveform_dictionary

    waveform_kwargs = dict(
        waveform_approximant='NRSur7dq4Inertial', reference_frequency=60.0,
        minimum_frequency=0.0, maximum_frequency=frequency_array[-1],
        catch_waveform_errors=False, pn_spin_order=-1, pn_tidal_order=-1,
        pn_phase_order=-1, pn_amplitude_order=0)
    waveform_kwargs.update(kwargs)

    return _base_lal_cbc_fd_waveform(
        frequency_array=frequency_array, mass_1=mass_1, mass_2=mass_2,
        luminosity_distance=luminosity_distance, theta_jn=theta_jn, phase=phase,
        a_1=a_1, a_2=a_2, tilt_1=tilt_1, tilt_2=tilt_2, phi_12=phi_12, phi_jl=phi_jl,
        lambda_1=0.0, lambda_2=0.0, eccentricity=0.0, **waveform_kwargs)


try:
    import pycbc
except ImportError:
    logger.warning('Cannot import PyCBC, not import PyCBC-related function, quitting.')
    exit()

from pycbc.waveform.waveform import get_td_waveform, _check_lal_pars
from pycbc.types import TimeSeries
from pycbc import pnutils

from lalsimulation import NRSur7dq4Inertial

def pycbc_lalsim_NRSur_inertial_td_waveform(**p):
    # Default value is True in PyCBC
    fail_tolerant_waveform_generation = False

    # Implement based on pycbc.waveform.waveform._lalsim_td_waveform
    lal_pars = _check_lal_pars(p)

    inertial_mode_array = p.get('inertial_mode_array', None)
    if inertial_mode_array is None:
        logger.warning('Inertial modes not specified, considering using the regular `NRSur7dq4` instead.')
    inertial_mode_array_lal = lalsim.SimInspiralCreateModeArray()
    for mode in inertial_mode_array:
        lalsim.SimInspiralModeArrayActivateMode(inertial_mode_array_lal, mode[0], mode[1])
    lalsim.SimInspiralWaveformParamsInsertInertialModeArray(lal_pars, inertial_mode_array_lal)

    #nonGRparams can be straightforwardly added if needed, however they have to
    # be invoked one by one
    try:
        hp1, hc1 = lalsim.SimInspiralChooseTDWaveform(
               float(pnutils.solar_mass_to_kg(p['mass1'])),
               float(pnutils.solar_mass_to_kg(p['mass2'])),
               float(p['spin1x']), float(p['spin1y']), float(p['spin1z']),
               float(p['spin2x']), float(p['spin2y']), float(p['spin2z']),
               pnutils.megaparsecs_to_meters(float(p['distance'])),
               float(p['inclination']), float(p['coa_phase']),
               float(p['long_asc_nodes']), float(p['eccentricity']), float(p['mean_per_ano']),
               float(p['delta_t']), float(p['f_lower']), float(p['f_ref']),
               lal_pars,
               NRSur7dq4Inertial)
    except RuntimeError:
        if not fail_tolerant_waveform_generation:
            raise

    hp = TimeSeries(hp1.data.data[:], delta_t=hp1.deltaT, epoch=hp1.epoch)
    hc = TimeSeries(hc1.data.data[:], delta_t=hc1.deltaT, epoch=hc1.epoch)

    return hp, hc

# This may not be necessary??
pycbc.waveform.add_custom_waveform('NRSur7dq4Inertial', pycbc_lalsim_NRSur_inertial_td_waveform,
                                   'time', force=True)
