## Regarding timing of our waveforms

All models presented here are time domain models, and there is a certain level of complexity to make sure the waveform to be at the right `geocent_time`. 

In all these models, we adapt the following architecture:
1. Chose an arbitrary `t_start`, that is reasonably long, and at the same time can remove the junk radiation at the beginning of the waveform. Note that this `t_start` is different from model to model, and depends on the simulation of the type of signal. 
2. Then generate waveform from the NR data, and trim it to the appropriate length.
3. Finally, pad it with zeros before and after to ensure the total length matches with the desired length of the given `time_array`. This "padding" is achieved by rolling the waveform such that the peak of the (2,2) mode of the waveform matches with the provided `geocent_time`. This would, however, introduce a small artefact when `geocent_time` is too close to `start_time` of the `time_array`, in such case, the beginning segment of the waveform would be rolled to the end of the waveform instead.

## Time shift in `Bilby`
One small note about the treatment of `geocent_time` in Bilby *(took a full day to discover this issue)*:  
Since Bilby bases on mostly *frequency domain model*, in their waveform models, e.g.: [`bilby.gw.source.lal_binary_black_hole`][bilby:bbh], do not consider the input `geocent_time`, even at the level of their `WaveformGenerator`. By default, if a time domain strain is requested from those models, they would simply align the peak of the waveform at the beginning of the `time_array`. As it turns out, alignment with `geocent_time` is only considered when the waveform is injected into the interferometer object. This is wildly different from our models here, which do respect the input `geocent_time`. Consequently, when they are being injected into a Bilby interferometer, the result waveform would have be shifted twice. To encounter this, we add an option `gt0=True` in the `waveform_arguments` of these models to be compatible with Bilby.  
For more details about this issue, feel free to have a look at [here](bilby-extra/others/waveform_timeing_issue.html), where the whole journey of discovering this problem is displayed.

[bilby:bbh]: https://lscsoft.docs.ligo.org/bilby/_modules/bilby/gw/source.html#lal_binary_black_hole
