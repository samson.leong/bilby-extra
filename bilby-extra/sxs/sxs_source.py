import numpy as np
import h5py
import lal
from lal import PC_SI, MSUN_SI, MTSUN_SI, PI
import lalsimulation as lalsim

try:
    from sxs.utilities.lvcnr import Dataset
except ImportError:
    from ..utils import logger
    logger.warning('Cannot import SXS-related function, skipping.')
    quit()

from .sxs_utils import get_h5file_path


def lal_SXS_tref_binary_black_hole(
    frequency_array, total_mass, iota, phase, luminosity_distance,
    **waveform_kwargs):

    sxs_code = waveform_kwargs['sxs_code']
    reference_time = waveform_kwargs.get('reference_time', -100)
    minimum_frequency = waveform_kwargs.get('minimum_frequency', None)
    maximum_frequency = waveform_kwargs.get('maximum_frequency', frequency_array[-1])
    max_ell = waveform_kwargs.get('max_ell', 4)
    delta_f = np.diff(frequency_array)[0]

    # Find and read the NR file
    NR_filename = get_h5file_path(sxs_code)
    NR_file = h5py.File(NR_filename, 'r')

    # Initialise modes less than max_ell in LAL mode-array
    lal_params = lal.CreateDict()
    mode_arr = lalsim.SimInspiralCreateModeArray()
    for ell in range(2, max_ell+1):
        for em in range(-ell, ell+1):
            lalsim.SimInspiralModeArrayActivateMode(mode_arr, ell, em)
    lalsim.SimInspiralWaveformParamsInsertModeArray(lal_params, mode_arr)
    lalsim.SimInspiralWaveformParamsInsertNumRelData(lal_params, NR_filename)

    # Obtain reference_frequency and freq. bounds
    reference_omega = Dataset.read(NR_file['Omega-vs-time']).spline(reference_time)
    reference_frequency = reference_omega / (total_mass * MTSUN_SI * PI)
    frequency_bounds = ((frequency_array >= minimum_frequency) *
                        (frequency_array <= maximum_frequency))

    # NR simulation properties
    nr_mass_1 = NR_file.attrs['mass1']
    nr_mass_2 = NR_file.attrs['mass2']
    nr_total_mass = nr_mass_1 + nr_mass_2
    mass_1 = nr_mass_1 * nr_total_mass / total_mass
    mass_2 = nr_mass_2 * nr_total_mass / total_mass
    mass_1_SI = mass_1 * MSUN_SI
    mass_2_SI = mass_2 * MSUN_SI
    luminosity_distance_SI = luminosity_distance * 1e6 * PC_SI

    nr_minimum_frequency = NR_file.attrs['f_lower_at_1MSUN'] / total_mass
    if minimum_frequency is None:
        minimum_frequency = nr_minimum_frequency
    else:
        # make sure the requested f_min is larger than what NR can provide
        assert nr_minimum_frequency < minimum_frequency

    # LAL-frame spins at chosen reference time (freq.) from NR
    component_spins = lalsim.SimInspiralNRWaveformGetSpinsFromHDF5File(
        reference_frequency, total_mass, NR_filename)

    # TODO: FIX THIS :)
    # This is a temporary solution, but the root of this has yet to be found
    # But at least it can be localised to the XLALSimIMRNRWaveformGetModes function
    # Since both HpHc and Hlms give the same error
    Mtot_sq = total_mass * total_mass

    hp, hc = lalsim.SimInspiralFD(
        mass_1_SI, mass_2_SI, *component_spins, luminosity_distance_SI, iota, phase,
        0, 0, 0, delta_f, minimum_frequency*Mtot_sq, maximum_frequency,
        reference_frequency*Mtot_sq, lal_params, lalsim.NR_hdf5)

    h_plus = np.zeros_like(frequency_array, dtype=complex)
    h_cross = np.zeros_like(frequency_array, dtype=complex)

    h_plus[:len(hp.data.data)] = hp.data.data
    h_cross[:len(hc.data.data)] = hc.data.data

    h_plus *= frequency_bounds
    h_cross *= frequency_bounds

    dt = 1 / hp.deltaF + (hp.epoch.gpsSeconds + hp.epoch.gpsNanoSeconds * 1e-9)
    time_shift = np.exp(-1j * 2 * np.pi * dt * frequency_array[frequency_bounds])
    h_plus[frequency_bounds] *= time_shift
    h_cross[frequency_bounds] *= time_shift

    return {'plus': h_plus, 'cross': h_cross}


def lal_SXS_tref_binary_black_hole_TD(
    time_array, total_mass, iota, phase, luminosity_distance,
    **waveform_kwargs):

    sxs_code = waveform_kwargs['sxs_code']
    reference_time = waveform_kwargs.get('reference_time', -100)
    minimum_frequency = waveform_kwargs.get('minimum_frequency', None)
    max_ell = waveform_kwargs.get('max_ell', 4)
    delta_t = np.diff(time_array)[0]

    # Find and read the NR file
    NR_filename = get_h5file_path(sxs_code)
    NR_file = h5py.File(NR_filename, 'r')

    # Initialise modes less than max_ell in LAL mode-array
    lal_params = lal.CreateDict()
    mode_arr = lalsim.SimInspiralCreateModeArray()
    for ell in range(2, max_ell+1):
        for em in range(-ell, ell+1):
            lalsim.SimInspiralModeArrayActivateMode(mode_arr, ell, em)
    lalsim.SimInspiralWaveformParamsInsertModeArray(lal_params, mode_arr)
    lalsim.SimInspiralWaveformParamsInsertNumRelData(lal_params, NR_filename)

    # Obtain reference_frequency and freq. bounds
    reference_omega = Dataset.read(NR_file['Omega-vs-time']).spline(reference_time)
    reference_frequency = reference_omega / (total_mass * MTSUN_SI * PI)

    # NR simulation properties
    nr_mass_1 = NR_file.attrs['mass1']
    nr_mass_2 = NR_file.attrs['mass2']
    nr_total_mass = nr_mass_1 + nr_mass_2
    mass_1 = nr_mass_1 * nr_total_mass / total_mass
    mass_2 = nr_mass_2 * nr_total_mass / total_mass
    mass_1_SI = mass_1 * MSUN_SI
    mass_2_SI = mass_2 * MSUN_SI
    luminosity_distance_SI = luminosity_distance * 1e6 * PC_SI

    nr_minimum_frequency = NR_file.attrs['f_lower_at_1MSUN'] / total_mass
    if minimum_frequency is None:
        minimum_frequency = nr_minimum_frequency
    else:
        # make sure the requested f_min is larger than what NR can provide
        assert nr_minimum_frequency < minimum_frequency

    # LAL-frame spins at chosen reference time (freq.) from NR
    component_spins = lalsim.SimInspiralNRWaveformGetSpinsFromHDF5File(
        reference_frequency, total_mass, NR_filename)

    # TODO: FIX THIS :)
    # This is a temporary solution, but the root of this has yet to be found
    # But at least it can be localised to the XLALSimIMRNRWaveformGetModes function
    # Since both HpHc and Hlms give the same error
    Mtot_sq = total_mass * total_mass

    h_plus, h_cross = lalsim.SimInspiralNRWaveformGetHplusHcross(
        phase, iota, delta_t/Mtot_sq, mass_1_SI, mass_2_SI, luminosity_distance_SI,
        minimum_frequency*Mtot_sq, reference_frequency*Mtot_sq,
        *component_spins, NR_filename, mode_arr)

    return {'plus': h_plus, 'cross': h_cross}
