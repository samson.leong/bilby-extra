import glob

# TODO: Remove this and change to source global variables.
working_dir = '/users/hin-wai.leong/Projects/kick_related/GW190412/'

def get_h5file_path(sxs_code):
    NR_filenames = glob.glob(working_dir+f'SXS_BBH_{sxs_code}/LVC_NR/SXS_BBH_{sxs_code}_Res?.h5')
    if len(NR_filenames) < 1:
        raise FileNotFoundError
    elif len(NR_filenames) > 1:
        raise IOError('More than one NR-files are found.')
    else:
        NR_filename = NR_filenames[0]
    return NR_filename
