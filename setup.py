#!/usr/bin/env python

from setuptools import setup
import subprocess
import sys
import os

python_version = sys.version_info
if python_version < (3, 7):
    sys.exit("Python < 3.7 is not supported, aborting setup")
print("Confirmed Python version {}.{}.{} >= 3.7.0".format(*python_version[:3]))


def write_version_file(version):
    """ Writes a file with version information to be used at run time

    Parameters
    ----------
    version: str
        A string containing the current version information

    Returns
    -------
    version_file: str
        A path to the version file

    """
    try:
        git_log = subprocess.check_output(
            ["git", "log", "-1", "--pretty=%h %ai"]
        ).decode("utf-8")
        git_diff = (
            subprocess.check_output(["git", "diff", "."])
            + subprocess.check_output(["git", "diff", "--cached", "."])
        ).decode("utf-8")
        if git_diff == "":
            git_status = "(CLEAN) " + git_log
        else:
            git_status = "(UNCLEAN) " + git_log
    except Exception as e:
        print("Unable to obtain git version information, exception: {}".format(e))
        git_status = ""

    version_file = ".version"
    if os.path.isfile(version_file) is False:
        with open("bilby-extra/" + version_file, "w+") as f:
            f.write("{}: {}".format(version, git_status))

    return version_file


def get_long_description():
    """ Finds the README and reads in the description """
    here = os.path.abspath(os.path.dirname(__file__))
    with open(os.path.join(here, "README.rst")) as f:
        long_description = f.read()
    return long_description


# get version info from __init__.py
def readfile(filename):
    with open(filename) as fp:
        filecontents = fp.read()
    return filecontents


packages = [
    "bilby_extra",
    "bilby_extra.utils",
    "bilby_extra.proca",
    "bilby_extra.modGR",
    "bilby_extra.eccentric",
    "bilby_extra.reference_time",
    "bilby_extra.sxs",
    "bilby_extra.inertial",
]
package_dir = {
    "bilby_extra": "bilby-extra",
    "bilby_extra.utils": "bilby-extra/utils",
    "bilby_extra.proca": "bilby-extra/proca",
    "bilby_extra.modGR": "bilby-extra/modGR",
    "bilby_extra.eccentric": "bilby-extra/eccentric",
    "bilby_extra.reference_time": "bilby-extra/reference_time",
    "bilby_extra.sxs": "bilby-extra/sxs",
    "bilby_extra.inertial": "bilby-extra/inertial",
}

try:
    from lalsimulation import \
        SimInspiralPrecessingNRSur_get_omega_ref, \
        SimInspiralTransformPrecessingNewInitialConditions
except ImportError:
    err_message = "Unable to import customised functions from LALSimulation, " + \
                    "will not import reference_time module."
    print(err_message)
    packages.remove("bilby_extra.reference_time")
    package_dir.pop("bilby_extra.reference_time")

try:
    from lalsimulation import \
        SimInspiralWaveformParamsLookupInertialModeArray, \
        SimInspiralWaveformParamsInsertInertialModeArray
except ImportError:
    err_message = "Unable to import customised functions from LALSimulation, " + \
                    "will not import inertial module."
    print(err_message)
    packages.remove("bilby_extra.inertial")
    package_dir.pop("bilby_extra.inertial")

VERSION = "0.1.0"
version_file = write_version_file(VERSION)
long_description = get_long_description()

setup(
    name="bilby_extra",
    description="Extra waveform models that work with Bilby.",
    long_description=long_description,
    long_description_content_type="text/x-rst",
    author="samson.leong",
    author_email="samson.leong@ligo.org",
    license="MIT",
    version=VERSION,
    packages=packages,
    package_dir=package_dir,
    package_data={
        "bilby_extra": [version_file],
        "bilby_extra.proca": [],
        "bilby_extra.eccentric": ["data/TEOBResumS/*", "data/TEOBResumS/build/*", "data/SEOBNRE_S/*"],
    },
    python_requires=">=3.5",
    install_requires=["bilby", "numpy>=1.9"],
    classifiers=[
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
