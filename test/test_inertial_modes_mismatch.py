#!/users/hin-wai.leong/.conda/envs/170324_py310_NRSurInertial/lib/python3.10
import os
os.environ['LAL_DATA_PATH'] = '/users/hin-wai.leong/src/lalsuite-extra/data/lalsimulation/'

import numpy as np
from bilby_extra.inertial import lal_inertial_binary_black_hole

from lal import PI, MSUN_SI, CreateDict, SpinWeightedSphericalHarmonic
from lalsimulation import SimInspiralCreateModeArray, \
        SimInspiralModeArrayActivateMode, \
        SimInspiralWaveformParamsInsertInertialModeArray, \
        SphHarmTimeSeriesGetMode, \
        SimInspiralPrecessingNRSurModes, \
        SimInspiralPrecessingNRSurInertialPolarizations, \
        NRSur7dq4, NRSur7dq4Inertial

inertial_mode_array = [
        [2, 2], [2, -2], [3, 3], [3, -3]
        ]

sampling_frequency = 4096
mass_1 = 100 * MSUN_SI
mass_2 = 80  * MSUN_SI
spin_1x, spin_1y, spin_1z = 0.7, 0.2, 0.3
spin_2x, spin_2y, spin_2z = 0.2, 0.6, -0.4
minimum_frequency = 0
reference_frequency = 40
luminosity_distance = 100 # m
iota, phase = 0.123, 0.456
lal_waveform_dictionary = CreateDict()

Ylm = lambda iota, phase, ell, em: SpinWeightedSphericalHarmonic(
        iota, 0.5 * PI - phase, -2, ell, em)

print('First combing inertial modes by hand')

hlms = SimInspiralPrecessingNRSurModes(
        1/sampling_frequency, mass_1, mass_2,
        spin_1x, spin_1y, spin_1z,
        spin_2x, spin_2y, spin_2z,
        minimum_frequency, reference_frequency,
        luminosity_distance, lal_waveform_dictionary,
        NRSur7dq4)

hphc = 0 + 1j * 0
# First construct from individual modes
for ell, em in inertial_mode_array:
    hlm = SphHarmTimeSeriesGetMode(hlms, ell, em).data.data
    hphc += hlm * Ylm(iota, phase, ell, em)

hp1 = hphc.real
hc1 = hphc.imag * -1
ta1 = np.arange(len(hp1)) / sampling_frequency

print('Now compute the polarisation using the new function')

inertial_mode_array_lal = SimInspiralCreateModeArray()
for mode in inertial_mode_array:
    SimInspiralModeArrayActivateMode(inertial_mode_array_lal, mode[0], mode[1])
SimInspiralWaveformParamsInsertInertialModeArray(lal_waveform_dictionary, inertial_mode_array_lal)

_hp2, _hc2 = SimInspiralPrecessingNRSurInertialPolarizations(
            phase, iota, 1/sampling_frequency,
            mass_1, mass_2, luminosity_distance,
            minimum_frequency, reference_frequency,
            spin_1x, spin_1y, spin_1z,
            spin_2x, spin_2y, spin_2z,
            lal_waveform_dictionary, NRSur7dq4Inertial
        )
hp2 = _hp2.data.data
hc2 = _hc2.data.data
ta2 = np.arange(len(hp2)) / sampling_frequency

import matplotlib.pyplot as plt

# Turns out we don't need this, we have a more stringent test :p
# # We need to compute the match between two signals in time-domain
# # The simplest way to define one such object is through their inner product:
# def time_domain_inner_product(
#         ts_1, ts_2, time_array=None, deltaT=None):
#     if deltaT is None and time_array is None:
#         raise IOError('Either a (uniform) time array or the deltaT needs to be provided.')
#     elif deltaT is None:
#         deltaT = np.diff(time_array)[0]

#     integrand = np.conjugate(ts_1) * ts_2

#     return np.real(np.sum(integrand)) * deltaT

tol = 1e-12
hp_isclose = np.all(np.isclose(hp1, hp2,
                      rtol=tol, atol=tol*1e-3))
hc_isclose = np.all(np.isclose(hc1, hc2,
                      rtol=tol, atol=tol*1e-3))
print(f'Tolerance is {tol:.3e}')
print(f'Minimum value of |h_+|: {np.min(np.abs(hp1)):.3e}')
print(f'Is h_+ close -- {hp_isclose}')
print(f'Minimum value of |h_×|: {np.min(np.abs(hc1)):.3e}')
print(f'Is h_× close -- {hc_isclose}')

fig, axes = plt.subplots(2, 1, figsize=(10, 8), constrained_layout=True)
axes[0].plot(ta1, hp1, ls='-', label=r'$h_+$, Manual sum modes')
axes[0].plot(ta2, hp2, ls='--', label=r'$h_+$, NRSur7dq4Inertial')
axes[1].plot(ta1, hc1, ls='-', label=r'$h_\times$, Manual sum modes')
axes[1].plot(ta2, hc2, ls='--', label=r'$h_\times$, NRSur7dq4Inertial')

for ax in axes:
    ax.set_xlabel(r'Time\,/\,s')
    ax.legend()
axes[0].set_ylabel(r'$h_+ (100\,{\rm metre})$')
axes[1].set_ylabel(r'$h_\times (100\,{\rm metre})$')

fig.savefig('ManualSum_vs_NRSurInertial.png', dpi=200)
fig.savefig('ManualSum_vs_NRSurInertial.pdf')
