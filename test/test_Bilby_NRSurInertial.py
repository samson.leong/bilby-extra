#!/usr/local/bin python3

import os
import numpy as np
import matplotlib.pyplot as plt
from bilby.gw.waveform_generator import WaveformGenerator
from bilby.gw.source import lal_binary_black_hole

from bilby_extra.inertial import lal_inertial_binary_black_hole

from lal import PC_SI
MPC_SI = 1e6 * PC_SI

plt.style.use(os.environ['KICK_REPO']+'matplotlibrc')

parameters = {
    'mass_1': 60, 'mass_2': 50,
    'a_1': 0.5, 'a_2': 0.3,
    'tilt_1': 0.3, 'tilt_2': 0.5,
    'phi_12': 0.5, 'phi_jl': 0.9,
    'theta_jn': 0.9, 'phase': 0.5,
    'luminosity_distance': 1/MPC_SI
}

ell2_mode_array = [
    (2,-2), (2,-1), (2,0), (2,1), (2,2) ]

pm2_mode_array = [
    (2,-2), (2,2) ]

nrsur_wfm_args = {
    'waveform_approximant': 'NRSur7dq4',
    'reference_frequency': 20,
    'minimum_frequency': 0,
    'mode_array': ell2_mode_array
}

sampling_frequency = 4096
wfm_gen_kwargs = dict(
    sampling_frequency=sampling_frequency, duration=8,
    parameters=parameters)

# 1. Compuare full ell=2 modes
gen_nrsur_ell2 = WaveformGenerator(
    frequency_domain_source_model=lal_binary_black_hole,
    waveform_arguments=nrsur_wfm_args,
    **wfm_gen_kwargs )
nrsur_ell2_td_hphc = gen_nrsur_ell2.time_domain_strain()

# Keep the same mode_arrays
inertial_wfm_args = nrsur_wfm_args.copy()
inertial_wfm_args['waveform_approximant'] = 'NRSur7dq4Inertial'
inertial_wfm_args['inertial_mode_array'] = ell2_mode_array

gen_inertial_ell2 = WaveformGenerator(
    frequency_domain_source_model=lal_inertial_binary_black_hole,
    waveform_arguments=inertial_wfm_args,
    **wfm_gen_kwargs )
inertial_ell2_td_hphc = gen_inertial_ell2.time_domain_strain()

# 2. Compuare (2, ±2) modes
nrsur_pm2_wfm_args = nrsur_wfm_args.copy()
nrsur_pm2_wfm_args['mode_array'] = pm2_mode_array

gen_nrsur_pm2 = WaveformGenerator(
    frequency_domain_source_model=lal_binary_black_hole,
    waveform_arguments=nrsur_pm2_wfm_args,
    **wfm_gen_kwargs )
nrsur_pm2_td_hphc = gen_nrsur_pm2.time_domain_strain()

# Generate all co-orbital modes, we do not modify mode_array
inertial_pm2_wfm_args = nrsur_wfm_args.copy()
inertial_pm2_wfm_args['waveform_approximant'] = 'NRSur7dq4Inertial'
inertial_pm2_wfm_args['inertial_mode_array'] = pm2_mode_array

gen_inertial_pm2 = WaveformGenerator(
    frequency_domain_source_model=lal_inertial_binary_black_hole,
    waveform_arguments=inertial_pm2_wfm_args,
    **wfm_gen_kwargs )
inertial_pm2_td_hphc = gen_inertial_pm2.time_domain_strain()

fig, axes = plt.subplots(4, 2, figsize=(17, 14),
                         height_ratios=[3.6,1,3.6,1],
                         layout='constrained')

startT = 4-1.5
endT = 4.2

ax = axes[0,0]
ax.plot(gen_nrsur_ell2.time_array,
        np.roll(nrsur_ell2_td_hphc['plus'], 4*sampling_frequency),
        ls='-', c='C0', label='Co-orbital modes')
ax.plot(gen_inertial_ell2.time_array,
        np.roll(inertial_ell2_td_hphc['plus'], 4*sampling_frequency),
        ls='-.', c='C2', label='Inertial modes')
ax.set_ylabel(r'$h_+$')
ax.set_title(r'All $\ell = 2$ modes')
ax.legend()

ax = axes[0,1]
ax.plot(gen_nrsur_ell2.time_array,
        np.roll(nrsur_ell2_td_hphc['cross'], 4*sampling_frequency),
        ls='-', c='C0', label='Co-orbital modes')
ax.plot(gen_inertial_ell2.time_array,
        np.roll(inertial_ell2_td_hphc['cross'], 4*sampling_frequency),
        ls='-.', c='C2', label='Inertial modes')
ax.set_ylabel(r'$h_\times$')
ax.set_title(r'All $\ell = 2$ modes')

ax = axes[1,0]
ax.plot(gen_inertial_ell2.time_array,
        np.roll(nrsur_ell2_td_hphc['plus'], 4*sampling_frequency) -
        np.roll(inertial_ell2_td_hphc['plus'], 4*sampling_frequency),
        ls='-', c='k')
ax.set_ylabel(r'$\Delta h_+$ (Co-orbital - Inertial)')

ax = axes[1,1]
ax.plot(gen_inertial_ell2.time_array,
        np.roll(nrsur_ell2_td_hphc['cross'], 4*sampling_frequency) -
        np.roll(inertial_ell2_td_hphc['cross'], 4*sampling_frequency),
        ls='-', c='k')
ax.set_ylabel(r'$\Delta h_\times$ (Co-orbital - Inertial)')

ax = axes[2,0]
ax.plot(gen_nrsur_ell2.time_array,
        np.roll(nrsur_pm2_td_hphc['plus'], 4*sampling_frequency),
        ls='-', c='C0', label='Co-orbital modes')
ax.plot(gen_inertial_ell2.time_array,
        np.roll(inertial_pm2_td_hphc['plus'], 4*sampling_frequency),
        ls='-.', c='C2', label='Inertial modes')
ax.set_ylabel(r'$h_+$')
ax.set_title(r'(2, $\pm 2$) modes')
ax.legend()

ax = axes[2,1]
ax.plot(gen_nrsur_ell2.time_array,
        np.roll(nrsur_pm2_td_hphc['cross'], 4*sampling_frequency),
        ls='-', c='C0', label='Co-orbital modes')
ax.plot(gen_inertial_ell2.time_array,
        np.roll(inertial_pm2_td_hphc['cross'], 4*sampling_frequency),
        ls='-.', c='C2', label='Inertial modes')
ax.set_ylabel(r'$h_\times$')
ax.set_title(r'(2, $\pm 2$) modes')

ax = axes[3,0]
ax.plot(gen_inertial_pm2.time_array,
        np.roll(nrsur_pm2_td_hphc['plus'], 4*sampling_frequency) -
        np.roll(inertial_pm2_td_hphc['plus'], 4*sampling_frequency),
        ls='-', c='k')
ax.set_ylabel(r'$\Delta h_+$ (Co-orbital - Inertial)')

ax = axes[3,1]
ax.plot(gen_inertial_pm2.time_array,
        np.roll(nrsur_pm2_td_hphc['cross'], 4*sampling_frequency) -
        np.roll(inertial_pm2_td_hphc['cross'], 4*sampling_frequency),
        ls='-', c='k')
ax.set_ylabel(r'$\Delta h_\times$ (Co-orbital - Inertial)')

for ax in axes.flatten():
    ax.set_xlabel(r'Time $t$/s')
    ax.set_xlim(startT, endT)

fig.savefig('Compare_Bilby_NRSurInertial.pdf')
fig.savefig('Compare_Bilby_NRSurInertial.png', dpi=200)
