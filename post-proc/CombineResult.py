#!/usr/bin/env python3

import os
from pandas import DataFrame as df

from bilby.core.utils import (
    decode_bilby_json,
    check_directory_exists_and_if_not_mkdir,
    logger, infer_parameters_from_function,
    latex_plot_format, safe_save_figure,
    BilbyJsonEncoder, 
    move_old_file, get_version_information,
     docstring,
    recursively_save_dict_contents_to_group,
    recursively_load_dict_contents_from_group,
    recursively_decode_bilby_json,
)

def _determine_file_name(filename, outdir, identifier):
    """ Helper method to determine the filename """
    if filename is not None:
        return filename
    else:
        if (outdir is None) and (label is None):
            raise ValueError("No information given to load file")
        else:
            return os.path.join(outdir, f'{label}_result.json')

class stats_value():
    def __init__(self, label, median=None, lower=0, upper=0, 
            interval=(5, 95), posterior=None, scale=1, unit=''):
        self.label = label
        self.interval = interval
        self.posterior = posterior
        self.scale = scale
        self.unit = unit

        if median is not None:
            self.median = median
            self.lower = lower
            self.upper = upper
        elif posterior is not None:
            triplet = np.percentile(posterior, [interval[0], 50, interval[95]])
            self.median = triplet[1]
            self.lower = triplet[0]
            self.upper = triplet[2]
        else:
            raise ValueError("At least one of median or posterior is required for a valid stats_val object.")

        self.upper_range = self.upper - self.median
        self.lower_range = self.median - self.lower

    def __repr__(self):
        lw_per = self.interval[0]
        up_per = self.interval[1]
        med = self.median
        up = self.upper
        lw = self.lower
        upr = self.upper_range
        lwr = self.lower_range
        sci_scale = r"$\times 10^{{{}}}$".format(np.log10(self.scale) if self.scale != 1 else ""

        return (
            "Statistics of {}:\n"
            "{:d}%: {:e}{}{}\n"
            "Median: {:5.3e} (+{:4.3e}, -{:4.3e}){}{}\n"
            "{:d}%: {:e}{}{}\n"
            .format(self.label,
                lw_per, lw*scale, sci_scale, self.unit,
                med*scale, upr*scale, lwr*scale, sci_scale, self.unit,
                up_per, up*scale, sci_scale, self.unit)
            )

    def __str__(self):
        sci_scale = r"$\times 10^{{{}}}$".format(np.log10(self.scale) if self.scale != 1 else ""
        return r"{}: ${{{:.2f}}}_{{-{:.2f}}}^{{+{:.2f}}}${}{}".format(
            self.median, self.lower_range, self.upper_range, sci_scale, self.unit)



class CombinedResult:
    def __init__(self, event_name, strain, indir=None, outdir=None,
                 meta_data=None, posterior=None, samples=None,
                 log_evidence=np.nan,
                 log_evidence_err=np.nan, 
                 log_noise_evidence=np.nan, log_bayes_factor=np.nan,
                 parameter_labels=None, parameter_labels_with_unit=None,
                 version=None):
        self.event_name = event_name
        self.strain = strain
        self.ratio = "Unequal" if strain != 'equal' else 'Equal'

        self.indir = indir
        self.outdir = outdir
        self.posterior = posterior
        self.samples = samples
        self.log_evidence = log_evidence
        self.log_evidence_err = log_evidence_err
        self.information_gain = information_gain
        self.log_noise_evidence = log_noise_evidence
        self.log_bayes_factor = log_bayes_factor

        self.identifier = self.set_identifier()
        self.geocent_time = self.set_geocent_time()

    def set_identifier(self)
        if strain == "unequal":
            identifier = f'{self.event_name}_{self.ratio}_all'
        elif strain == "everything":
            identifier = f'{self.event_name}_{self.strain}_ALL'
        else:
            identifier = f'{self.event_name}_{self.ratio}_{self.strain}'
        return identifier

    def set_geocent_time(self)
        '''Obtain the geocent time of a given event name from GraceDb.

        Parameters
        ----------
        event_name: str
                The name of the event, in the format 'S999999xx'.
        '''

        event_name_ = self.event_name
        event_name = event_name_.replace('_U', '')
        event_name = event_name_.replace('_P', '')

        try:
            from ligo.gracedb.rest import GraceDb
        except ImportError:
            import subprocess

            output = subprocess.check_output(
                ["gracedb get superevent "+event_name], shell=True)
            event_json = json.loads(output)

        else:
            client = GraceDb()
            event_dict = client.superevent(event_name).json()

        return event_json['t_0']

    @classmethod
    def from_json(cls, filename=None, outdir=None, identifier=None):
        filename = _determine_file_name(filename, outdir, identifier)

        if os.path.isfile(filename):
            with open(filename, 'r') as file:
                dictionary = json.load(file, object_hook=decode_bilby_json)
                try:
                    return cls(**dictionary)
                except TypeError as e:
                    raise IOError("Unable to load dictionary, error={}".format(e))
        else:
            raise IOError("No result '{}' found".format(filename))


class Result(object):
    def __init__(self, label='no_label', outdir='.', sampler=None,
                 search_parameter_keys=None, fixed_parameter_keys=None,
                 constraint_parameter_keys=None, priors=None,
                 sampler_kwargs=None, injection_parameters=None,
                 meta_data=None, posterior=None, samples=None,
                 nested_samples=None, log_evidence=np.nan,
                 log_evidence_err=np.nan, information_gain=np.nan,
                 log_noise_evidence=np.nan, log_bayes_factor=np.nan,
                 log_likelihood_evaluations=None,
                 log_prior_evaluations=None, sampling_time=None, nburn=None,
                 num_likelihood_evaluations=None, walkers=None,
                 max_autocorrelation_time=None, use_ratio=None,
                 parameter_labels=None, parameter_labels_with_unit=None,
                 version=None):
        """ A class to store the results of the sampling run

        Parameters
        ==========
        label, outdir, sampler: str
            The label, output directory, and sampler used
        search_parameter_keys, fixed_parameter_keys, constraint_parameter_keys: list
            Lists of the search, constraint, and fixed parameter keys.
            Elements of the list should be of type `str` and match the keys
            of the `prior`
        priors: dict, bilby.core.prior.PriorDict
            A dictionary of the priors used in the run
        sampler_kwargs: dict
            Key word arguments passed to the sampler
        injection_parameters: dict
            A dictionary of the injection parameters
        meta_data: dict
            A dictionary of meta data to store about the run
        posterior: pandas.DataFrame
            A pandas data frame of the posterior
        samples, nested_samples: array_like
            An array of the output posterior samples and the unweighted samples
        log_evidence, log_evidence_err, log_noise_evidence, log_bayes_factor: float
            Natural log evidences
        information_gain: float
            The Kullback-Leibler divergence
        log_likelihood_evaluations: array_like
            The evaluations of the likelihood for each sample point
        num_likelihood_evaluations: int
            The number of times the likelihood function is called
        log_prior_evaluations: array_like
            The evaluations of the prior for each sample point
        sampling_time: float
            The time taken to complete the sampling
        nburn: int
            The number of burn-in steps discarded for MCMC samplers
        walkers: array_like
            The samplers taken by a ensemble MCMC samplers
        max_autocorrelation_time: float
            The estimated maximum autocorrelation time for MCMC samplers
        use_ratio: bool
            A boolean stating whether the likelihood ratio, as opposed to the
            likelihood was used during sampling
        parameter_labels, parameter_labels_with_unit: list
            Lists of the latex-formatted parameter labels
        version: str,
            Version information for software used to generate the result. Note,
            this information is generated when the result object is initialized

        Notes
        =========
        All sampling output parameters, e.g. the samples themselves are
        typically not given at initialisation, but set at a later stage.

        """

        self.label = label
        self.outdir = os.path.abspath(outdir)
        self.sampler = sampler
        self.search_parameter_keys = search_parameter_keys
        self.fixed_parameter_keys = fixed_parameter_keys
        self.constraint_parameter_keys = constraint_parameter_keys
        self.parameter_labels = parameter_labels
        self.parameter_labels_with_unit = parameter_labels_with_unit
        self.priors = priors
        self.sampler_kwargs = sampler_kwargs
        self.meta_data = meta_data
        self.injection_parameters = injection_parameters
        self.posterior = posterior
        self.samples = samples
        self.nested_samples = nested_samples
        self.walkers = walkers
        self.nburn = nburn
        self.use_ratio = use_ratio
        self.log_evidence = log_evidence
        self.log_evidence_err = log_evidence_err
        self.information_gain = information_gain
        self.log_noise_evidence = log_noise_evidence
        self.log_bayes_factor = log_bayes_factor
        self.log_likelihood_evaluations = log_likelihood_evaluations
        self.log_prior_evaluations = log_prior_evaluations
        self.num_likelihood_evaluations = num_likelihood_evaluations
        self.sampling_time = sampling_time
        self.version = version
        self.max_autocorrelation_time = max_autocorrelation_time

        self.prior_values = None
        self._kde = None

    @classmethod
    def _from_hdf5_old(cls, filename=None, outdir=None, label=None):
        """ Read in a saved .h5 data file in the old format.

        Parameters
        ==========
        filename: str
            If given, try to load from this filename
        outdir, label: str
            If given, use the default naming convention for saved results file

        Returns
        =======
        result: bilby.core.result.Result

        Raises
        =======
        ValueError: If no filename is given and either outdir or label is None
                    If no bilby.core.result.Result is found in the path

        """
        import deepdish
        filename = _determine_file_name(filename, outdir, label, 'hdf5', False)

        if os.path.isfile(filename):
            dictionary = deepdish.io.load(filename)
            # Some versions of deepdish/pytables return the dictionary as
            # a dictionary with a key 'data'
            if len(dictionary) == 1 and 'data' in dictionary:
                dictionary = dictionary['data']

            if "priors" in dictionary:
                # parse priors from JSON string (allowing for backwards
                # compatibility)
                if not isinstance(dictionary["priors"], PriorDict):
                    try:
                        priordict = PriorDict()
                        for key, value in dictionary["priors"].items():
                            if key not in ["__module__", "__name__", "__prior_dict__"]:
                                try:
                                    priordict[key] = decode_bilby_json(value)
                                except AttributeError:
                                    continue
                        dictionary["priors"] = priordict
                    except Exception as e:
                        raise IOError(
                            "Unable to parse priors from '{}':\n{}".format(
                                filename, e,
                            )
                        )
            try:
                if isinstance(dictionary.get('posterior', None), dict):
                    dictionary['posterior'] = pd.DataFrame(dictionary['posterior'])
                return cls(**dictionary)
            except TypeError as e:
                raise IOError("Unable to load dictionary, error={}".format(e))
        else:
            raise IOError("No result '{}' found".format(filename))

    _load_doctstring = """ Read in a saved .{format} data file

    Parameters
    ==========
    filename: str
        If given, try to load from this filename
    outdir, label: str
        If given, use the default naming convention for saved results file

    Returns
    =======
    result: bilby.core.result.Result

    Raises
    =======
    ValueError: If no filename is given and either outdir or label is None
                If no bilby.core.result.Result is found in the path

    """

    @staticmethod
    @docstring(_load_doctstring.format(format="pickle"))
    def from_pickle(filename=None, outdir=None, label=None):
        filename = _determine_file_name(filename, outdir, label, 'hdf5', False)
        import dill
        with open(filename, "rb") as ff:
            return dill.load(ff)

    @classmethod
    @docstring(_load_doctstring.format(format="hdf5"))
    def from_hdf5(cls, filename=None, outdir=None, label=None):
        import h5py
        filename = _determine_file_name(filename, outdir, label, 'hdf5', False)
        with h5py.File(filename, "r") as ff:
            data = recursively_load_dict_contents_from_group(ff, '/')
        if list(data.keys()) == ["data"]:
            return cls._from_hdf5_old(filename=filename)
        data["posterior"] = pd.DataFrame(data["posterior"])
        data["priors"] = PriorDict._get_from_json_dict(
            json.loads(data["priors"], object_hook=decode_bilby_json)
        )
        try:
            cls = getattr(import_module(data['__module__']), data['__name__'])
        except ImportError:
            logger.debug(
                "Module {}.{} not found".format(data["__module__"], data["__name__"])
            )
        except KeyError:
            logger.debug("No class specified, using base Result.")
        for key in ["__module__", "__name__"]:
            if key in data:
                del data[key]
        return cls(**data)

    @classmethod
    @docstring(_load_doctstring.format(format="json"))
    def from_json(cls, filename=None, outdir=None, label=None, gzip=False):
        filename = _determine_file_name(filename, outdir, label, 'json', gzip)

        if os.path.isfile(filename):
            dictionary = load_json(filename, gzip)
            try:
                return cls(**dictionary)
            except TypeError as e:
                raise IOError("Unable to load dictionary, error={}".format(e))
        else:
            raise IOError("No result '{}' found".format(filename))

    def __str__(self):
        """Print a summary """
        if getattr(self, 'posterior', None) is not None:
            if getattr(self, 'log_noise_evidence', None) is not None:
                return ("nsamples: {:d}\n"
                        "ln_noise_evidence: {:6.3f}\n"
                        "ln_evidence: {:6.3f} +/- {:6.3f}\n"
                        "ln_bayes_factor: {:6.3f} +/- {:6.3f}\n"
                        .format(len(self.posterior), self.log_noise_evidence, self.log_evidence,
                                self.log_evidence_err, self.log_bayes_factor,
                                self.log_evidence_err))
            else:
                return ("nsamples: {:d}\n"
                        "ln_evidence: {:6.3f} +/- {:6.3f}\n"
                        .format(len(self.posterior), self.log_evidence, self.log_evidence_err))
        else:
            return ''

    @property
    def meta_data(self):
        return self._meta_data

    @meta_data.setter
    def meta_data(self, meta_data):
        if meta_data is None:
            meta_data = dict()
        meta_data = recursively_decode_bilby_json(meta_data)
        self._meta_data = meta_data

    @property
    def priors(self):
        if self._priors is not None:
            return self._priors
        else:
            raise ValueError('Result object has no priors')

    @priors.setter
    def priors(self, priors):
        if isinstance(priors, dict):
            if isinstance(priors, PriorDict):
                self._priors = priors
            else:
                self._priors = PriorDict(priors)
            if self.parameter_labels is None:
                self.parameter_labels = [self.priors[k].latex_label for k in
                                         self.search_parameter_keys]
            if self.parameter_labels_with_unit is None:
                self.parameter_labels_with_unit = [
                    self.priors[k].latex_label_with_unit for k in
                    self.search_parameter_keys]
        elif priors is None:
            self._priors = priors
            self.parameter_labels = self.search_parameter_keys
            self.parameter_labels_with_unit = self.search_parameter_keys
        else:
            raise ValueError("Input priors not understood")

    @property
    def samples(self):
        """ An array of samples """
        if self._samples is not None:
            return self._samples
        else:
            raise ValueError("Result object has no stored samples")

    @samples.setter
    def samples(self, samples):
        self._samples = samples

    @property
    def num_likelihood_evaluations(self):
        """ number of likelihood evaluations """
        if self._num_likelihood_evaluations is not None:
            return self._num_likelihood_evaluations
        else:
            raise ValueError("Result object has no stored likelihood evaluations")

    @num_likelihood_evaluations.setter
    def num_likelihood_evaluations(self, num_likelihood_evaluations):
        self._num_likelihood_evaluations = num_likelihood_evaluations

    @property
    def nested_samples(self):
        """" An array of unweighted samples """
        if self._nested_samples is not None:
            return self._nested_samples
        else:
            raise ValueError("Result object has no stored nested samples")

    @nested_samples.setter
    def nested_samples(self, nested_samples):
        self._nested_samples = nested_samples

    @property
    def walkers(self):
        """" An array of the ensemble walkers """
        if self._walkers is not None:
            return self._walkers
        else:
            raise ValueError("Result object has no stored walkers")

    @walkers.setter
    def walkers(self, walkers):
        self._walkers = walkers

    @property
    def nburn(self):
        """" An array of the ensemble walkers """
        if self._nburn is not None:
            return self._nburn
        else:
            raise ValueError("Result object has no stored nburn")

    @nburn.setter
    def nburn(self, nburn):
        self._nburn = nburn

    @property
    def posterior(self):
        """ A pandas data frame of the posterior """
        if self._posterior is not None:
            return self._posterior
        else:
            raise ValueError("Result object has no stored posterior")

    @posterior.setter
    def posterior(self, posterior):
        self._posterior = posterior

    @property
    def log_10_bayes_factor(self):
        return self.log_bayes_factor / np.log(10)

    @property
    def log_10_evidence(self):
        return self.log_evidence / np.log(10)

    @property
    def log_10_evidence_err(self):
        return self.log_evidence_err / np.log(10)

    @property
    def log_10_noise_evidence(self):
        return self.log_noise_evidence / np.log(10)

    @property
    def version(self):
        return self._version

    @version.setter
    def version(self, version):
        if version is None:
            self._version = 'bilby={}'.format(utils.get_version_information())
        else:
            self._version = version

    def _get_save_data_dictionary(self):
        # This list defines all the parameters saved in the result object
        save_attrs = [
            'label', 'outdir', 'sampler', 'log_evidence', 'log_evidence_err',
            'log_noise_evidence', 'log_bayes_factor', 'priors', 'posterior',
            'injection_parameters', 'meta_data', 'search_parameter_keys',
            'fixed_parameter_keys', 'constraint_parameter_keys',
            'sampling_time', 'sampler_kwargs', 'use_ratio', 'information_gain',
            'log_likelihood_evaluations', 'log_prior_evaluations',
            'num_likelihood_evaluations', 'samples', 'nested_samples',
            'walkers', 'nburn', 'parameter_labels', 'parameter_labels_with_unit',
            'version']
        dictionary = OrderedDict()
        for attr in save_attrs:
            try:
                dictionary[attr] = getattr(self, attr)
            except ValueError as e:
                logger.debug("Unable to save {}, message: {}".format(attr, e))
                pass
        return dictionary

    def save_to_file(self, filename=None, overwrite=False, outdir=None,
                     extension='json', gzip=False):
        """

        Writes the Result to a file.

        Supported formats are: `json`, `hdf5`, `arviz`, `pickle`

        Parameters
        ==========
        filename: optional,
            Filename to write to (overwrites the default)
        overwrite: bool, optional
            Whether or not to overwrite an existing result file.
            default=False
        outdir: str, optional
            Path to the outdir. Default is the one stored in the result object.
        extension: str, optional {json, hdf5, pkl, pickle, True}
            Determines the method to use to store the data (if True defaults
            to json)
        gzip: bool, optional
            If true, and outputting to a json file, this will gzip the resulting
            file and add '.gz' to the file extension.
        """

        if extension is True:
            extension = "json"

        outdir = self._safe_outdir_creation(outdir, self.save_to_file)
        if filename is None:
            filename = result_file_name(outdir, self.label, extension, gzip)

        move_old_file(filename, overwrite)

        # Convert the prior to a string representation for saving on disk
        dictionary = self._get_save_data_dictionary()

        # Convert callable sampler_kwargs to strings
        if dictionary.get('sampler_kwargs', None) is not None:
            for key in dictionary['sampler_kwargs']:
                if hasattr(dictionary['sampler_kwargs'][key], '__call__'):
                    dictionary['sampler_kwargs'][key] = str(dictionary['sampler_kwargs'])

        try:
            # convert priors to JSON dictionary for both JSON and hdf5 files
            if extension == 'json':
                dictionary["priors"] = dictionary["priors"]._get_json_dict()
                if gzip:
                    import gzip
                    # encode to a string
                    json_str = json.dumps(dictionary, cls=BilbyJsonEncoder).encode('utf-8')
                    with gzip.GzipFile(filename, 'w') as file:
                        file.write(json_str)
                else:
                    with open(filename, 'w') as file:
                        json.dump(dictionary, file, indent=2, cls=BilbyJsonEncoder)
            elif extension == 'hdf5':
                import h5py
                dictionary["__module__"] = self.__module__
                dictionary["__name__"] = self.__class__.__name__
                with h5py.File(filename, 'w') as h5file:
                    recursively_save_dict_contents_to_group(h5file, '/', dictionary)
            elif extension == 'pkl':
                import dill
                with open(filename, "wb") as ff:
                    dill.dump(self, ff)
            else:
                raise ValueError("Extension type {} not understood".format(extension))
        except Exception as e:
            import dill
            filename = ".".join(filename.split(".")[:-1]) + ".pkl"
            with open(filename, "wb") as ff:
                dill.dump(self, ff)
            logger.error(
                "\n\nSaving the data has failed with the following message:\n"
                "{}\nData has been dumped to {}.\n\n".format(e, filename)
            )

    def save_posterior_samples(self, filename=None, outdir=None, label=None):
        """ Saves posterior samples to a file

        Generates a .dat file containing the posterior samples and auxillary
        data saved in the posterior. Note, strings in the posterior are
        removed while complex numbers will be given as absolute values with
        abs appended to the column name

        Parameters
        ==========
        filename: str
            Alternative filename to use. Defaults to
            outdir/label_posterior_samples.dat
        outdir, label: str
            Alternative outdir and label to use

        """
        if filename is None:
            if label is None:
                label = self.label
            outdir = self._safe_outdir_creation(outdir, self.save_posterior_samples)
            filename = '{}/{}_posterior_samples.dat'.format(outdir, label)
        else:
            outdir = os.path.dirname(filename)
            self._safe_outdir_creation(outdir, self.save_posterior_samples)

        # Drop non-numeric columns
        df = self.posterior.select_dtypes([np.number]).copy()

        # Convert complex columns to abs
        for key in df.keys():
            if np.any(np.iscomplex(df[key])):
                complex_term = df.pop(key)
                df.loc[:, key + "_abs"] = np.abs(complex_term)
                df.loc[:, key + "_angle"] = np.angle(complex_term)

        logger.info("Writing samples file to {}".format(filename))
        df.to_csv(filename, index=False, header=True, sep=' ')

    def get_latex_labels_from_parameter_keys(self, keys):
        """ Returns a list of latex_labels corresponding to the given keys

        Parameters
        ==========
        keys: list
            List of strings corresponding to the desired latex_labels

        Returns
        =======
        list: The desired latex_labels

        """
        latex_labels = []
        for key in keys:
            if key in self.search_parameter_keys:
                idx = self.search_parameter_keys.index(key)
                label = self.parameter_labels_with_unit[idx]
            elif key in self.parameter_labels:
                label = key
            else:
                label = None
                logger.debug(
                    'key {} not a parameter label or latex label'.format(key)
                )
            if label is None:
                label = key.replace("_", " ")
            latex_labels.append(label)
        return latex_labels

    @property
    def covariance_matrix(self):
        """ The covariance matrix of the samples the posterior """
        samples = self.posterior[self.search_parameter_keys].values
        return np.cov(samples.T)

    @property
    def posterior_volume(self):
        """ The posterior volume """
        if self.covariance_matrix.ndim == 0:
            return np.sqrt(self.covariance_matrix)
        else:
            return 1 / np.sqrt(np.abs(np.linalg.det(
                1 / self.covariance_matrix)))

    @staticmethod
    def prior_volume(priors):
        """ The prior volume, given a set of priors """
        return np.prod([priors[k].maximum - priors[k].minimum for k in priors])

    def occam_factor(self, priors):
        """ The Occam factor,

        See Chapter 28, `Mackay "Information Theory, Inference, and Learning
        Algorithms" <http://www.inference.org.uk/itprnn/book.html>`_ Cambridge
        University Press (2003).

        """
        return self.posterior_volume / self.prior_volume(priors)

    @property
    def bayesian_model_dimensionality(self):
        """ Characterises how many parameters are effectively constraint by the data

        See <https://arxiv.org/abs/1903.06682>

        Returns
        =======
        float: The model dimensionality
        """
        return 2 * (np.mean(self.posterior['log_likelihood']**2) -
                    np.mean(self.posterior['log_likelihood'])**2)

    def get_one_dimensional_median_and_error_bar(self, key, fmt='.2f',
                                                 quantiles=(0.16, 0.84)):
        """ Calculate the median and error bar for a given key

        Parameters
        ==========
        key: str
            The parameter key for which to calculate the median and error bar
        fmt: str, ('.2f')
            A format string
        quantiles: list, tuple
            A length-2 tuple of the lower and upper-quantiles to calculate
            the errors bars for.

        Returns
        =======
        summary: namedtuple
            An object with attributes, median, lower, upper and string

        """
        summary = namedtuple('summary', ['median', 'lower', 'upper', 'string'])

        if len(quantiles) != 2:
            raise ValueError("quantiles must be of length 2")

        quants_to_compute = np.array([quantiles[0], 0.5, quantiles[1]])
        quants = np.percentile(self.posterior[key], quants_to_compute * 100)
        summary.median = quants[1]
        summary.plus = quants[2] - summary.median
        summary.minus = summary.median - quants[0]

        fmt = "{{0:{0}}}".format(fmt).format
        string_template = r"${{{0}}}_{{-{1}}}^{{+{2}}}$"
        summary.string = string_template.format(
            fmt(summary.median), fmt(summary.minus), fmt(summary.plus))
        return summary

    @latex_plot_format
    def plot_single_density(self, key, prior=None, cumulative=False,
                            title=None, truth=None, save=True,
                            file_base_name=None, bins=50, label_fontsize=16,
                            title_fontsize=16, quantiles=(0.16, 0.84), dpi=300):
        """ Plot a 1D marginal density, either probability or cumulative.

        Parameters
        ==========
        key: str
            Name of the parameter to plot
        prior: {bool (True), bilby.core.prior.Prior}
            If true, add the stored prior probability density function to the
            one-dimensional marginal distributions. If instead a Prior
            is provided, this will be plotted.
        cumulative: bool
            If true plot the CDF
        title: bool
            If true, add 1D title of the median and (by default 1-sigma)
            error bars. To change the error bars, pass in the quantiles kwarg.
            See method `get_one_dimensional_median_and_error_bar` for further
            details). If `quantiles=None` is passed in, no title is added.
        truth: {bool, float}
            If true, plot self.injection_parameters[parameter].
            If float, plot this value.
        save: bool:
            If true, save plot to disk.
        file_base_name: str, optional
            If given, the base file name to use (by default `outdir/label_` is
            used)
        bins: int
            The number of histogram bins
        label_fontsize, title_fontsize: int
            The fontsizes for the labels and titles
        quantiles: tuple
            A length-2 tuple of the lower and upper-quantiles to calculate
            the errors bars for.
        dpi: int
            Dots per inch resolution of the plot

        Returns
        =======
        figure: matplotlib.pyplot.figure
            A matplotlib figure object
        """
        import matplotlib.pyplot as plt
        logger.info('Plotting {} marginal distribution'.format(key))
        label = self.get_latex_labels_from_parameter_keys([key])[0]
        fig, ax = plt.subplots()
        try:
            ax.hist(self.posterior[key].values, bins=bins, density=True,
                    histtype='step', cumulative=cumulative)
        except ValueError as e:
            logger.info(
                'Failed to generate 1d plot for {}, error message: {}'
                .format(key, e))
            return
        ax.set_xlabel(label, fontsize=label_fontsize)
        if truth is not None:
            ax.axvline(truth, ls='-', color='orange')

        summary = self.get_one_dimensional_median_and_error_bar(
            key, quantiles=quantiles)
        ax.axvline(summary.median - summary.minus, ls='--', color='C0')
        ax.axvline(summary.median + summary.plus, ls='--', color='C0')
        if title:
            ax.set_title(summary.string, fontsize=title_fontsize)

        if isinstance(prior, Prior):
            theta = np.linspace(ax.get_xlim()[0], ax.get_xlim()[1], 300)
            if cumulative is False:
                ax.plot(theta, prior.prob(theta), color='C2')
            else:
                ax.plot(theta, prior.cdf(theta), color='C2')

        if save:
            fig.tight_layout()
            if cumulative:
                file_name = file_base_name + key + '_cdf'
            else:
                file_name = file_base_name + key + '_pdf'
            safe_save_figure(fig=fig, filename=file_name, dpi=dpi)
            plt.close(fig)
        else:
            return fig

    def plot_marginals(self, parameters=None, priors=None, titles=True,
                       file_base_name=None, bins=50, label_fontsize=16,
                       title_fontsize=16, quantiles=(0.16, 0.84), dpi=300,
                       outdir=None):
        """ Plot 1D marginal distributions

        Parameters
        ==========
        parameters: (list, dict), optional
            If given, either a list of the parameter names to include, or a
            dictionary of parameter names and their "true" values to plot.
        priors: {bool (False), bilby.core.prior.PriorDict}
            If true, add the stored prior probability density functions to the
            one-dimensional marginal distributions. If instead a PriorDict
            is provided, this will be plotted.
        titles: bool
            If true, add 1D titles of the median and (by default 1-sigma)
            error bars. To change the error bars, pass in the quantiles kwarg.
            See method `get_one_dimensional_median_and_error_bar` for further
            details). If `quantiles=None` is passed in, no title is added.
        file_base_name: str, optional
            If given, the base file name to use (by default `outdir/label_` is
            used)
        bins: int
            The number of histogram bins
        label_fontsize, title_fontsize: int
            The font sizes for the labels and titles
        quantiles: tuple
            A length-2 tuple of the lower and upper-quantiles to calculate
            the errors bars for.
        dpi: int
            Dots per inch resolution of the plot
        outdir: str, optional
            Path to the outdir. Default is the one store in the result object.

        Returns
        =======
        """
        if isinstance(parameters, dict):
            plot_parameter_keys = list(parameters.keys())
            truths = parameters
        elif parameters is None:
            plot_parameter_keys = self.posterior.keys()
            if self.injection_parameters is None:
                truths = dict()
            else:
                truths = self.injection_parameters
        else:
            plot_parameter_keys = list(parameters)
            if self.injection_parameters is None:
                truths = dict()
            else:
                truths = self.injection_parameters

        if file_base_name is None:
            outdir = self._safe_outdir_creation(outdir, self.plot_marginals)
            file_base_name = '{}/{}_1d/'.format(outdir, self.label)
            check_directory_exists_and_if_not_mkdir(file_base_name)

        if priors is True:
            priors = getattr(self, 'priors', dict())
        elif isinstance(priors, dict):
            pass
        elif priors in [False, None]:
            priors = dict()
        else:
            raise ValueError('Input priors={} not understood'.format(priors))

        for i, key in enumerate(plot_parameter_keys):
            if not isinstance(self.posterior[key].values[0], float):
                continue
            prior = priors.get(key, None)
            truth = truths.get(key, None)
            for cumulative in [False, True]:
                self.plot_single_density(
                    key, prior=prior, cumulative=cumulative, title=titles,
                    truth=truth, save=True, file_base_name=file_base_name,
                    bins=bins, label_fontsize=label_fontsize, dpi=dpi,
                    title_fontsize=title_fontsize, quantiles=quantiles)

    # Direct copy from Bilby (bilby/core/result.py), with some modifications.
    def plot_corner(self, self, parameters=None, titles=True, 
            filename=None, dpi=300, **kwargs):

        default_param = ['luminosity_distance', incline, 'ra', 'dec']
        default_labels = [r'$d_L$ [Mpc]', r'$\theta_{JN}$ [rad]', 'RA [rad]', 'DEC [rad]']

        # bilby default corner kwargs. Overwritten by anything passed to kwargs
        defaults_kwargs = dict(
                bins=50, smooth=0.9, label_kwargs=dict(fontsize=16),
                title_kwargs=dict(fontsize=16), color='#0072C1',
                truth_color='tab:orange', quantiles=[0.16, 0.84],
                levels=(1 - np.exp(-0.5), 1 - np.exp(-2), 1 - np.exp(-9 / 2.)),
                plot_density=False, plot_datapoints=True, fill_contours=True,
                max_n_ticks=3, hist_kwargs=dict(density=True))

        defaults_kwargs.update(kwargs)
        kwargs = defaults_kwargs

        plot_parameter_keys = list(parameters) if parameters is not None else default_param

        # Get latex formatted strings for the plot labels
        if isinstance(parameters, dict):
            kwargs['labels'] = [parameters[key] for key in plot_parameter_keys]
        else:
            kwargs['labels'] = kwargs.get('labels', default_labels)

        # Unless already set, set the range to include all samples
        # This prevents ValueErrors being raised for parameters with no range
        kwargs['range'] = [1] * len(plot_parameter_keys)

        # Create the data array to plot and pass everything to corner
        xs = self.posterior[plot_parameter_keys]
        fig = corner.corner(xs, **kwargs)
        axes = fig.get_axes()

        # Extract the directory name from the indir
        basename = os.path.basename(self.indir[:-1])
        plot_title = 'Joint Posterior of {} (from {})'.format(self.identifier, basename)
        plt.rc('text.latex', preamble=r'\usepackage{underscore}')
        plt.suptitle(plot_title, x=0.4, y=0.95, ha='left', fontsize=27)

        map_txt = 'Max. a Posteriori: {:.2f} ({})\n'.format(self.max_MaP['value'], self.max_MaP['simulation'])
        logL_txt = 'Max. ' + r'$\log\mathcal{L_\mathrm{max}}$' + ': {:.2f} ({})\n'.format(self.max_logL['value'], self.max_logL['simulation'])
        maxBF_txt = 'Max. ' + r'$\log\mathrm{BF_\mathrm{max}}$' + ': {:.2f} ({})\n'.format(self.max_logBF['value'], self.max_logBF['simulation'])
        bf_txt = 'Avg. ' + r'$\log\mathrm{BF_\mathrm{max}}$' + ': {:.2f}\n'.format(self.avg_logBF)
        z_Q1_range, z_Q3_range = np.diff(list(self['redshift'].values()))
        z_txt = r"Redshift: ${{{:.2f}}}_{{-{:.2f}}}^{{+{:.2f}}}$".format(
                self['redshift']['p50'], z_Q3_range, z_Q1_range)
        mu_Q1_range, mu_Q3_range = np.diff(list(self['boson_mass'].values()))
        mu_txt = r"Boson mass: ${{{:.2f}}}_{{-{:.2f}}}^{{+{:.2f}}} \times 10^{{-13}}$ eV".format(
                self['boson_mass']['p50']*1e13, mu_Q1_range*1e13, mu_Q3_range*1e13)
        info_txt = map_txt + logL_txt + maxBF_txt + bf_txt + z_txt +'\n' + mu_txt

        fig.text(0.65, 0.85, info_txt, fontsize=20, transform=fig.transFigure, ha='left', va='top')

        # Add the titles on each subplots
        if titles and kwargs.get('quantiles', None) is not None:
            for i, par in enumerate(plot_parameter_keys):
                ax = axes[i + i * len(plot_parameter_keys)]
                if ax.title.get_text() == '':
                    quantiles = np.array([kwargs['quantiles'][0], 0.5, kwargs['quantiles'][1]])
                    Q1, Q2, Q3 = np.percentile(posterior[par], quantiles * 100)
                    fmt = "{0:.3f}".format
                    string_template = r"${{{0}}}_{{-{1}}}^{{+{2}}}$"
                    quant_str = string_template.format(
                            fmt(Q2), fmt(Q2-Q1), fmt(Q3-Q2))
                    ax.set_title(quant_str, **kwargs['title_kwargs'])

        if filename is None:
            filename = '{}/joint_posterior_{}_corner.png'.format(self['outdir'], self['event_name'])
            logging.debug('Saving corner plot to {}'.format(filename))
        fig.savefig(filename, dpi=dpi)
        plt.close(fig)

        return fig


    @latex_plot_format
    def plot_with_data(self, model, x, y, ndraws=1000, npoints=1000,
                       xlabel=None, ylabel=None, data_label='data',
                       data_fmt='o', draws_label=None, filename=None,
                       maxl_label='max likelihood', dpi=300, outdir=None):
        """ Generate a figure showing the data and fits to the data

        Parameters
        ==========
        model: function
            A python function which when called as `model(x, **kwargs)` returns
            the model prediction (here `kwargs` is a dictionary of key-value
            pairs of the model parameters.
        x, y: np.ndarray
            The independent and dependent data to plot
        ndraws: int
            Number of draws from the posterior to plot
        npoints: int
            Number of points used to plot the smoothed fit to the data
        xlabel, ylabel: str
            Labels for the axes
        data_label, draws_label, maxl_label: str
            Label for the data, draws, and max likelihood legend
        data_fmt: str
            Matpltolib fmt code, defaults to `'-o'`
        dpi: int
            Passed to `plt.savefig`
        filename: str
            If given, the filename to use. Otherwise, the filename is generated
            from the outdir and label attributes.
        outdir: str, optional
            Path to the outdir. Default is the one store in the result object.

        """
        import matplotlib.pyplot as plt

        # Determine model_posterior, the subset of the full posterior which
        # should be passed into the model
        model_keys = infer_parameters_from_function(model)
        model_posterior = self.posterior[model_keys]

        xsmooth = np.linspace(np.min(x), np.max(x), npoints)
        fig, ax = plt.subplots()
        logger.info('Plotting {} draws'.format(ndraws))
        for _ in range(ndraws):
            s = model_posterior.sample().to_dict('records')[0]
            ax.plot(xsmooth, model(xsmooth, **s), alpha=0.25, lw=0.1, color='r',
                    label=draws_label)
        try:
            if all(~np.isnan(self.posterior.log_likelihood)):
                logger.info('Plotting maximum likelihood')
                s = model_posterior.iloc[self.posterior.log_likelihood.idxmax()]
                ax.plot(xsmooth, model(xsmooth, **s), lw=1, color='k',
                        label=maxl_label)
        except (AttributeError, TypeError):
            logger.debug(
                "No log likelihood values stored, unable to plot max")

        ax.plot(x, y, data_fmt, markersize=2, label=data_label)

        if xlabel is not None:
            ax.set_xlabel(xlabel)
        if ylabel is not None:
            ax.set_ylabel(ylabel)

        handles, labels = plt.gca().get_legend_handles_labels()
        by_label = OrderedDict(zip(labels, handles))
        plt.legend(by_label.values(), by_label.keys())
        ax.legend(numpoints=3)
        fig.tight_layout()
        if filename is None:
            outdir = self._safe_outdir_creation(outdir, self.plot_with_data)
            filename = '{}/{}_plot_with_data'.format(outdir, self.label)
        safe_save_figure(fig=fig, filename=filename, dpi=dpi)
        plt.close(fig)

    def calculate_prior_values(self, priors):
        """
        Evaluate prior probability for each parameter for each sample.

        Parameters
        ==========
        priors: dict, PriorDict
            Prior distributions
        """
        self.prior_values = pd.DataFrame()
        for key in priors:
            if key in self.posterior.keys():
                if isinstance(priors[key], DeltaFunction):
                    continue
                else:
                    self.prior_values[key]\
                        = priors[key].prob(self.posterior[key].values)

    @property
    def kde(self):
        """ Kernel density estimate built from the stored posterior

        Uses `scipy.stats.gaussian_kde` to generate the kernel density
        """
        if self._kde:
            return self._kde
        else:
            self._kde = scipy.stats.gaussian_kde(
                self.posterior[self.search_parameter_keys].values.T)
            return self._kde

    def posterior_probability(self, sample):
        """ Calculate the posterior probability for a new sample

        This queries a Kernel Density Estimate of the posterior to calculate
        the posterior probability density for the new sample.

        Parameters
        ==========
        sample: dict, or list of dictionaries
            A dictionary containing all the keys from
            self.search_parameter_keys and corresponding values at which to
            calculate the posterior probability

        Returns
        =======
        p: array-like,
            The posterior probability of the sample

        """
        if isinstance(sample, dict):
            sample = [sample]
        ordered_sample = [[s[key] for key in self.search_parameter_keys]
                          for s in sample]
        return self.kde(ordered_sample)

    def _safe_outdir_creation(self, outdir=None, caller_func=None):
        if outdir is None:
            outdir = self.outdir
        try:
            check_directory_exists_and_if_not_mkdir(outdir)
        except PermissionError:
            raise FileMovedError("Cannot write in the out directory.\n"
                                 "Did you move the here file from another system?\n"
                                 "Try calling " + caller_func.__name__ + " with the 'outdir' "
                                 "keyword argument, e.g. " + caller_func.__name__ + "(outdir='.')")
        return outdir

