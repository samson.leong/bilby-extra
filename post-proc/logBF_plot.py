#!/home/samson.leong/VirtualEnv/py42_Bilby/bin/python
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import sys
import socket
import glob
import json
import logging

event_name = sys.argv[1] if len(sys.argv) > 1 else 'S190519bj'

reco_list = ['reco3', 'reco4', 'reco15', 'reco41', 'reco57']
hostname = socket.getfqdn()
if "caltech" in hostname:
    html_path = "/home/samson.leong/public_html/MyVenv/ProcaStar/"
elif "iucaa" in hostname:
    html_path = "/home/samson.leong/public_html/ProcaStar/pBilby/ProcaRuns/"
indir =  '{}{}/Results/'.format(html_path, event_name)
dir_list = ['{}_Unequal_{}_plots/'.format(event_name, reco) for reco in reco_list]
dir_list.append('{}_Equal_equal_plots/'.format(event_name))
outdir = indir

param_list = []
for directory in dir_list:
    wild_path = indir+directory+"*result.json"
    print(wild_path)
    try:
        # Try to search for the json file.
        path = glob.glob(wild_path)[0]
    except IndexError:
        logging.warning("Cannot find this {}".format(wild_path))
        continue
    else:
        with open(path, 'r') as json_file:
            result_dict = json.load(json_file)
            param_list.extend(result_dict['param_logL_logBF'])
            # print(len(result_dict['param_logL_logBF']))

# print(len(param_list))
remove_w2 = [0.7256, 0.8536, 0.6218]
param_list = [row for row in param_list if row[1] not in remove_w2]
# print(len(param_list))

# Structure of param_list:
# [w_eco1, w_eco2, m_eco1, m_eco2, final_spin, logL, logBF]
w1 = [row[0] for row in param_list]
w2 = [row[1] for row in param_list]
m1 = [row[2] for row in param_list]
m2 = [row[3] for row in param_list]
af = [row[4] for row in param_list]
log_BF = [row[6] for row in param_list]
q = np.array([(val2 / val1 if val1 > val2 else val1 / val2) for val1, val2 in zip(m1, m2)])

# Flip w1, w2 if w1 < w2
for ind, (w1_v, w2_v) in enumerate(zip(w1, w2)):
    if w2_v > w1_v:
        w1[ind] = w2_v
        w2[ind] = w1_v
    else:
        pass

mpl.rcParams['xtick.labelsize'] = 12
mpl.rcParams['ytick.labelsize'] = 12

# I don't know a good way to place the colorbar in the middle of the plot,
# so I end up spliting the figure into 5 subplots and place the colorbar in the third one
# that way I can adjust the margins individually.
fig, (ax1, space1, cbar_ax, space2, ax2) = plt.subplots(1, 5, figsize=(18,7.5), gridspec_kw={'width_ratios': [11, 0.57, 0.75, 0.05, 11]}, constrained_layout=True)
# These two lines clear eveythiing from the two empty subplots (margins)
space1.axis('off')
space2.axis('off')

# Create two more figures for seperate plots
fig1, ax01 = plt.subplots(1, 1, figsize=(9,7.5), constrained_layout=True)
fig2, ax02 = plt.subplots(1, 1, figsize=(9,7.5), constrained_layout=True)

c_min, c_max = np.min(log_BF), np.max(log_BF)
area = np.array((log_BF - c_min)/ (c_max - c_min)) * 14 + 8
print(len(log_BF), len(area), len(w1))
print(area)

# Plotting (af, q)
for axis01 in [ax1, ax01]:
    axis01.scatter(af, q, s=area, c=log_BF, vmin=c_min-4, vmax=c_max+4, cmap='GnBu')
    axis01.set_xlabel(r'$a_f$', fontsize=14)
    axis01.set_ylabel(r'$q$', fontsize=14)
    axis01.set_title(r"$a_f$ vs $q$ -- {}".format(event_name), fontsize=20)
    plt.grid(False)

# Plotting (ω1, ω2)
xline = np.linspace(0, 1, 1000)
w_min = np.min([w1, w2])
w_max = np.max([w1, w2])
for axis02 in [ax2, ax02]:
    scatter = axis02.scatter(w1, w2, s=area, c=log_BF, vmin=c_min-4, vmax=c_max+4, cmap='GnBu')
    axis02.set_xlabel(r'$\omega_1$', fontsize=14)
    axis02.set_ylabel(r'$\omega_2$', fontsize=14)
    axis02.set_title(r"Oscillation frequency $\omega_1$ vs $\omega_2$ -- {}".format(event_name), fontsize=20)
    # Fill up half of the plot
    axis02.fill_between(xline, xline, 1, alpha=0.4, color='slategrey')
    axis02.set_xlim(w_min - 0.005, w_max + 0.005)
    axis02.set_ylim(w_min - 0.005, w_max + 0.005)
    plt.grid(False)

# The colourbar
for f, a in zip([fig, fig1, fig2], [cbar_ax, None, None]):
    cbar = f.colorbar(scatter, cax=a, format='%.0f')
    cbar.set_label(r'$\log\mathrm{{BF}}$', rotation=90, ha='right', fontsize=14)

fig.savefig(outdir+"logBF_spinNomega.pdf", bbox_inches='tight')
fig.savefig(outdir+"logBF_spinNomega.png", bbox_inches='tight')
fig1.savefig(outdir+"logBF_spin.pdf", bbox_inches='tight')
fig1.savefig(outdir+"logBF_spin.png", bbox_inches='tight')
fig2.savefig(outdir+"logBF_omega.pdf", bbox_inches='tight')
fig2.savefig(outdir+"logBF_omega.png", bbox_inches='tight')
plt.close('all')
