#!/home/samson.leong/VirtualEnv/py36_Bilby/bin/python
import numpy as np
from bilby.core.result import read_in_result as read
from bilby.core.utils import check_directory_exists_and_if_not_mkdir
from bilby import gw
from collections import OrderedDict as od
import matplotlib.pyplot as plt
import corner

import sys
import socket
import json
import logging
import argparse
import glob

# Setting things up
rc_params = {'backend': 'ps',
             'axes.labelsize': 11,
             'axes.titlesize': 10,
             'font.size': 11,
             'legend.fontsize': 10,
             'xtick.labelsize': 11,
             'ytick.labelsize': 11,
             'text.usetex': True,
             # 'font.sans-serif': ['Bitstream Vera Sans'],
             'font.family': 'Times New Roman'}

plt.rcParams.update(rc_params)
# matplotlib.rcParams['text.latex.preamble'] = [r'\usepackage{amsmath}']


# Allow passing keywords arguments to the script
# -n eventname, -s straininfo
# -m mode, -o occam
# -t testing
parser = argparse.ArgumentParser(description='External input options')

parser.add_argument('-n', '--eventname', dest="eventname", default='s190519bj',
                    help='The codename for a specific run, listed in "result_dir_list.py".')
parser.add_argument('-s', '--straininfo', dest="strain",
                    help='Specifying which list of strains to use.\nOptions:\n\
                    \t{equal:     equal_mass_info.json;\n\
                    \t eco15:   reco15_info.json;\n\
                    \t eco3:    reco3_info.json')
parser.add_argument('-m', '--mode', dest="mode", default=33,
                    help='Modes to include in the strain. Options: {22, 33}.')
parser.add_argument('-o', '--occam', dest="occam", type=bool, default=False,
                    help='Decide to calculate the Occam factor for each run.')
parser.add_argument('-t', '--testing', dest="test", type=bool, default=False,
                    help='Turn on testing mode, default = False')
parser.add_argument('-f', '--samplingfreq', dest="sampling_freq", type=int, default=1024,
                    help='Sampling frequency, default = 2048')
args = parser.parse_args()

use_mode = args.mode
event_name = args.eventname
ratio = "Unequal" if args.strain != 'equal' else 'Equal'
Com_or_Uni = 'Comoving' if '_U' not in event_name else 'Uniform'
strain_or_Psi = 'strain_' if '_P' not in event_name else ''

hostname = socket.getfqdn()
if "caltech" in hostname:
    project_home = "/home/samson.leong/Bilby/ProcaStar/"
    html_path = "/home/samson.leong/public_html/MyVenv/ProcaStar/"
    # Define how the json filenames generally look like.
    path_format = 'Mass_ratio_{strain}{{0}}_HLV_{freq}_DistanceFixed_TM_{dist}_result.json'.format(
                        strain=strain_or_Psi, freq=args.sampling_freq, dist=Com_or_Uni).format
    incline = "inclination"
    phase_ang = "azimuth"
elif "iucaa" in hostname:
    project_home = "/home/samson.leong/Projects/ProcaStar/"
    html_path = "/home/samson.leong/public_html/ProcaStar/pBilby/ProcaRuns/"
    path_format = 'ProcaStar_{strain}{{0}}_result.json'.format(
                    strain=strain_or_Psi).format
    incline = "theta_jn"
    phase_ang = "phase"

# path to the strain info files
strain_info_file = f"{args.strain}_info.json"
straininfo_file = f"{project_home}proca-star-pe/A_little_corner_containing_all_the_plotting_scripts/Production/simulations_info/" + strain_info_file

def geocent_time(event_name_):
    '''Obtain the geocent time of a given event name from GraceDb.

    Parameters
    ----------
    event_name: str
            The name of the event, in the format 'S999999xx'.
    '''

    event_name = event_name_.replace('_U', '')
    event_name = event_name_.replace('_P', '')

    try:
        from ligo.gracedb.rest import GraceDb
    except ImportError:
        import subprocess

        output = subprocess.check_output(
            ["gracedb get superevent "+event_name], shell=True)
        event_json = json.loads(output)
        gt = event_json['t_0']

    else:
        client = GraceDb()
        event_dict = client.superevent(event_name).json()
        gt = event_dict['t_0']

    return gt


# Some metadata of the event, the indir and outdir.
this_event = {'event_name': event_name,
              'indir': f'{html_path}{event_name}/',
              'outdir': f'{html_path}{event_name}/Results/',
              'event_t0': geocent_time(event_name)}

# make up a short string to identify the event and config
identifier = '{}_{}_{}'.format(event_name, ratio, args.strain)
if args.strain == "unequal":
    identifier = '{}_{}_all'.format(event_name, ratio)
elif args.strain == "everything":
    identifier = '{}_{}_ALL'.format(event_name, args.strain)
geocent_time = this_event['event_t0']
indir = this_event['indir'] + f'data/{ratio}mass/'
outdir = this_event['outdir'] + identifier + '_plots/'
check_directory_exists_and_if_not_mkdir(outdir)

# Redirect stdout to a log file to store it.
sys.stdout = open(outdir + identifier + '.log', "w")

# Start here :)
print('Event: {}, at t0 = {}\nGetting result from: {}\nSaving plots to: {}\n'.format(event_name, geocent_time, indir, outdir))

with open(straininfo_file) as strain_file:
    strain_info = json.load(strain_file)
    strain_file.close()

# Reduce the number of strains used in testing mode
if args.test:
    strain_info = {"Equal": {"v3_filt_20_eco2": [ 0.9, 0.72564, 0.78 ]},
                   "Unequal": {"reco15_eco2": [ 0.9, 0.9, 0.75, 0.72564, 0.78 ]}}[ratio]

## This was used as a filter to set the samples of mouses to 0.
## But now we simply exclude them from the game.
## # Remove some w in the equal mass case as those runs breaks our flat prior
## remove_w = {'equal': [0.87850, 0.88875, 0.89875, 0.90875, 0.91875, 0.92875]}\
##     .get(args.strain, [])

sample_sizes = []
BF_list = []
logL_list = []
rm_strain = []
all_paths = []
# Determine the max_BF and sample length first
for strain in strain_info:
    try:
        result = read(filename=indir+path_format(strain))
    except OSError:
        # try:
        #     wild_path = indir+f"*{strain}*.json"
        #     print("Trying to find this now: " + wild_path)
        #     # Try to do wild search for the json file instead.
        #     path = glob.glob(wild_path)[0]
        #     result = read(filename=path)
        # except IndexError:
            print(indir+path_format(strain))
            logging.warning(strain+' is still running.\nRemoving this entry.')
            rm_strain.append(strain)
        #     continue
        # else:
        #     all_paths.append(path)
    else:
        all_paths.append(indir+path_format(strain))
        poste = result.posterior
        sample_sizes.append(len(poste['log_likelihood']))
        BF_list.append(result.log_bayes_factor)
        logL_list.append(np.max(poste['log_likelihood']))

# Remove the entries hat does not exists from the list
# (increase efficiency of next loop, slightly)
for rm in rm_strain:
    strain_info.pop(rm, None)

reference_length = min(sample_sizes)
max_BF = max(BF_list)
max_logL = max(logL_list)

print("Among all Proca runs:")
print("Max logBF is {:f}".format(max_BF))
print("Max logL is {:f}".format(max_logL))

print("=====================================================")
print(f"(: {ratio} mass proca :)")
print("=====================================================")

# Prepare the lists for saving
xe, de, m1e, m2e, logle, iotae, phie, rae, dece, maxle, logbe, omegae1, omegae2, weffe, weffexe, afe, Mproca1, Mproca2, Occame, map_e\
    = [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], []

# Setting up the RA-DEC plots
ra_dec_plot = plt.figure(figsize=(5, 5.5))
ra_dec_gs = ra_dec_plot.add_gridspec(2, 2, wspace=0.05, hspace=0.05, width_ratios=[3, 1], height_ratios=[1, 3])
ra_dec_ax = ra_dec_plot.add_subplot(ra_dec_gs[1, 0], xlabel='RA [rad]', ylabel='DEC [rad]')
ra_dec_ax.minorticks_on()
ra_dec_ax.tick_params(direction='in', top=True, right=True)
ra_ax = ra_dec_plot.add_subplot(ra_dec_gs[0, 0], sharex=ra_dec_ax)
dec_ax = ra_dec_plot.add_subplot(ra_dec_gs[1, 1], sharey=ra_dec_ax)
for axis in [ra_ax, dec_ax]:
    axis.minorticks_on()
    axis.tick_params(which='both', direction='in', labelleft=False, labelbottom=False)
ra_dec_plot.suptitle('{} RA DEC plots'.format(identifier).replace("_", ' '))

# Convert dict to an Ordered Dict for key retrieval by index
strain_info = od(strain_info)

for ind, strain in enumerate(strain_info):
    print("==================={}=====================".format(strain))

    result = read(filename=all_paths[ind])

    poste = result.posterior
    log_BF = result.log_bayes_factor
    log_maxL = np.max(poste['log_likelihood'])

    # Maximum a Posteriori Probability
    this_map = poste['log_likelihood'] + poste['log_prior']
    map_e.append(np.max(this_map))
    # print(poste["log_likelihood"], poste['log_prior'])
    # ind_mape = this_map.idxmax()

    if args.occam:
        occam_fact = result.occam_factor(result.priors)
    else:
        occam_fact = 0

    if ratio == 'Unequal':
        w_eco1, w_eco2, m_eco1, m_eco2, final_spin_eco, w_eff_eco = strain_info[strain]
        q = min(m_eco2 / m_eco1, m_eco1 / m_eco2)
    else:
        w_eco1, m_eco1, final_spin_eco = strain_info[strain]
        q = 1
        m_eco2 = m_eco1
        w_eco2 = w_eco1
        w_eff_eco = w_eco1

    number_of_samples = np.max([0, np.int((reference_length*np.exp(log_BF-max_BF)-1))])

    selected_posterior_index = np.random.choice(range(len(poste)), number_of_samples, replace=False)

    ra_ax.hist(poste['ra'], density=True, alpha=0.15, bins=30, color='C0', histtype='step', ls='--', lw=0.85)
    dec_ax.hist(poste['dec'], density=True, alpha=0.15, bins=30, color='C0', histtype='step', orientation='horizontal', ls='--', lw=0.85)
    corner.hist2d(np.array(poste['ra']), np.array(poste['dec']), ax=ra_dec_ax,
                  plot_datapoints=False, plot_density=False, smooth=True,
                  levels=[0.9], fill_contours=False, bins=50, color='C0',
                  contour_kwargs= dict(alpha = 0.15, linestyles='--', linewidths=0.85))

    m1 = poste['total_mass'][selected_posterior_index]/(1+q)
    m2 = m1 * q

    m1e.extend(m1)
    m2e.extend(m2)
    de.extend(poste['luminosity_distance'][selected_posterior_index])
    xe.extend(poste['total_mass'][selected_posterior_index])
    rae.extend(poste['ra'][selected_posterior_index])
    dece.extend(poste['dec'][selected_posterior_index])
    logle.extend(poste['log_likelihood'][selected_posterior_index])
    iotae.extend(poste[incline][selected_posterior_index])
    phie.extend(poste[phase_ang][selected_posterior_index])

    maxle.append(log_maxL)
    logbe.append(log_BF)
    Occame.append(occam_fact)

    select_poste_idx_length = len(selected_posterior_index)
    omegae1.extend([w_eco1]*select_poste_idx_length)
    omegae2.extend([w_eco2]*select_poste_idx_length)
    weffe.extend([w_eff_eco]*select_poste_idx_length)
    afe.extend([final_spin_eco]*select_poste_idx_length)
    Mproca1.extend([m_eco1]*select_poste_idx_length)
    Mproca2.extend([m_eco2]*select_poste_idx_length)

    npm1 = np.array(m1)
    npm2 = np.array(m2)
    weffexe.extend(((w_eco1*npm1 + w_eco2*npm2) / (npm1 + npm2)).tolist())


    # print("Best Mass = "+str(poste['total_mass'][ind_mape]))
    # print("Max. a Posteriori = "+str(np.max(this_map)))
    print("Log MaxL = "+str(log_maxL))
    print("Log Bayes Factor = "+str(log_BF))
    print("Occam Factor = "+str(occam_fact))
    print("number of samples = "+str(number_of_samples))

ra_ax.hist(rae, density=True, alpha=0.7, bins=30, color='C3', histtype='step', ls='-', lw=0.9)
dec_ax.hist(dece, density=True, alpha=0.7, bins=30, color='C3', histtype='step', orientation='horizontal', ls='-', lw=0.9)
corner.hist2d(np.array(rae), np.array(dece), ax=ra_dec_ax,
              plot_datapoints=False, plot_density=False, smooth=True,
              levels=[0.9], fill_contours=False, bins=50, color='C3',
              contour_kwargs= dict(alpha = 0.7, linestyles='-', linewidths=0.9))
ra_dec_plot.savefig(outdir+identifier+'_radec_plot.pdf')
plt.close()

posterior_dict = {'luminosity_distance': de,
                  'total_mass': xe,
                  'mass_1': m1e,
                  'mass_2': m2e,
                  'ra': rae,
                  'dec': dece,
                  incline: iotae,
                  phase_ang: phie,
                  'proca_mass_1': Mproca1,
                  'proca_mass_2': Mproca2,
                  'omega_1': omegae1,
                  'omega_2': omegae2,
                  'omega_eff': weffe,
                  'final_spin': afe,
                  'log_likelihood': logle}

print("========================DONE=============================")
print("Average LogB for Proca = "+str(np.mean(logbe)))
print("Max MaP for Proca = "+str(np.max(map_e)))
print("Max Occam Factor for Proca = "+str(np.max(Occame)))
print("Min Occam Factor for Proca = "+str(np.min(Occame)))
print("################################################")
print("Average LogB for Proca = "+str(np.log(np.mean(np.exp(logbe)))))
print("Average Occam Factor for Proca = "+str(np.mean(Occame)))


print("=================Parameters Estimate======================")

def print_interval(interval_triplet):
    '''A handy function to print the given triplet.

    Parameters
    ----------
    interval_triplet: a triplet (list or tuple)
            A triple consists of the 5th, 50th, 95th percentile of a variable.
    '''

    tri05, tri50, tri95 = interval_triplet
    print("5%: {:e}".format(tri05))
    print("Median: {:5.3e} (+{:4.3e}, -{:4.3e})".format(tri50, tri95-tri50, tri50-tri05))
    print("95%: {:e}".format(tri95))


logging.info("... Loading ...\n--- Calculating Redshift ---\nConsider go get some coffee :)")
ze = gw.conversion.luminosity_distance_to_redshift(np.array(de))
source_frame_total_mass_e = np.array(xe) / (1. + ze)
source_frame_mass_1_e = np.array(m1e) / (1. + ze)
source_frame_mass_2_e = np.array(m2e) / (1. + ze)
posterior_dict["redshift"] = ze.tolist()
logging.info("!!!! Big News !!! \n We have calculated the redshift,\nlet you have a glimpse :)\n\
             The first one is: {} and \nhere is the last one: {}".format(ze[0], ze[-1]))
np_xe = np.array(xe)
np_afe = np.array(afe)
np_logle = np.array(logle)

print("----(Lab frame) Proca mass 90% intervals:")
m_interval = np.percentile(np_xe, [5,50, 95])
print_interval(m_interval)

print("----Redshift 90% intervals:")
ze_interval = np.percentile(ze, [5, 50, 95])
print_interval(ze_interval)

print("----(Source frame) Total mass 90% intervals:")
mfe_interval = np.percentile(source_frame_total_mass_e, [5, 50, 95])
print_interval(mfe_interval)

print("----(Source frame) mass_1 90% intervals:")
m1fe_interval = np.percentile(source_frame_mass_1_e, [5, 50, 95])
print_interval(m1fe_interval)

print("----(Source frame) mass_2 90% intervals:")
m2fe_interval = np.percentile(source_frame_mass_2_e, [5, 50, 95])
print_interval(m2fe_interval)

print("----μ_B mass 90% intervals:")
mu_B = 1.34*1e-10/np_xe*(np.array(Mproca1)+np.array(Mproca2))*(1+ze)
mu_interval = np.percentile(mu_B, [5, 50, 95])
print_interval(mu_interval)

print("----Mmax 90% intervals:")
mmax = 1.125 / mu_B * 1335 * 1e-13
mmax_interval = np.percentile(mmax, [5, 50, 95])
print_interval(mmax_interval)

print("----ω1 90% intervals:")
omegae1_interval = np.percentile(omegae1, [5, 50, 95])
print_interval(omegae1_interval)

print("----ω2 90% intervals:")
omegae2_interval = np.percentile(omegae2, [5, 50, 95])
print_interval(omegae2_interval)

print("----ωeff 90% intervals:")
weffe_interval = np.percentile(weffe, [5, 50, 95])
print_interval(weffe_interval)

print("----af 90% intervals:")
afe_interval = np.percentile(afe, [5, 50, 95])
print_interval(afe_interval)

print("----ι 90% intervals:")
iotae_interval = np.percentile(np.pi/2 - np.abs(np.pi/2 - np.array(iotae)), [5, 50, 95])
print_interval(iotae_interval)

strain_info_arr = np.stack(strain_info.values())
if ratio == 'Unequal':
    w_eco1s = strain_info_arr[:, 0]
    w_eco2s = strain_info_arr[:, 1]
    m_eco1s = strain_info_arr[:, 2]
    m_eco2s = strain_info_arr[:, 3]
    final_spin_ecos = strain_info_arr[:, 4]
    weff_ecos = strain_info_arr[:, 5]
else:
    w_eco1s = strain_info_arr[:, 0]
    m_eco1s = strain_info_arr[:, 1]
    final_spin_ecos = strain_info_arr[:, 2]
    w_eco2s = w_eco1s
    weff_ecos = w_eco1s
    m_eco2s = m_eco1s


def interval_dict(interval_triplet):
    '''A handy function to create a dictionary from the given triplet.

    Parameters
    ----------
    interval_triplet: a triplet (list or tuple)
            A triple consists of the 5th, 50th, 95th percentile of a variable.
    Returns:
    ----------
    dict: A dictioary with the three percentiles.
    '''

    tri05, tri50, tri95 = interval_triplet
    return {'p5': tri05, 'p50': tri50, 'p95': tri95}

# Create a dictionary of the result parameters for saving.
strain_info_keys = list(strain_info.keys())
posterior_dict['boson_mass'] = mu_B.tolist()
result_dict = {'identifier': identifier,
               'event_name': event_name,
               'mass_ratio': f'{ratio} mass',
               'indir': indir,
               'outdir': outdir,
               'avg_logBF': np.log(np.mean(np.exp(logbe))),
               'max_MaP': {'value': np.max(map_e), 'simulation': strain_info_keys[np.argmax(map_e)]},
               'max_logL': {'value': np.max(maxle), 'simulation': strain_info_keys[np.argmax(maxle)]},
               'max_logBF': {'value': np.max(logbe), 'simulation': strain_info_keys[np.argmax(logbe)]},
               'redshift': interval_dict(ze_interval),
               'source_frame_total_mass': interval_dict(mfe_interval),
               'source_frame_mass_1': interval_dict(m1fe_interval),
               'source_frame_mass_2': interval_dict(m2fe_interval),
               'total_proca_mass': interval_dict(m_interval),
               'max_proca_mass': interval_dict(mmax_interval),
               'boson_mass': interval_dict(mu_interval),
               'omega_1': interval_dict(omegae1_interval),
               'omega_2': interval_dict(omegae2_interval),
               'omega_eff': interval_dict(weffe_interval),
               'final_spin': interval_dict(afe_interval),
               'iota': interval_dict(iotae_interval),
               'posterior': posterior_dict,
               'param_logL_logBF': list(zip(w_eco1s, w_eco2s, m_eco1s, m_eco2s, final_spin_ecos, maxle, logbe))
               }

# write the result dictionary to a json file
json_name = identifier+'_result.json'
with open(outdir+json_name, "w") as json_file:
    json.dump(result_dict, json_file, indent=2)
    json_file.close
logging.info("Results are saved to '%s'", outdir+json_name)

############
# Plotting #
############
print("=======================Plotting===========================")

def scatterplot(xvar, yvar, xlab, ylab, savefilename, outdir=outdir, legend=None, xlim=None, ylim=None):
    ''' A function to make quick plots from the given two variables and store
    it in the default outdir.

    Parameters
    ----------
    xvar: array_like
        A list or np.array of the values of x variable.
    yvar: array_like
        A list or np.array of the values of y variable.
    xlab: str
        The label on the x-axis.
    ylab: str
        The label on the y-axis.
    savefilename: str
        The file name of the plot to be stored.
    legend: str (Optional, default=None)
        The labels to be used in the legend, if given.
    '''

    plt.scatter(xvar, yvar)
    plt.xlabel(xlab)
    plt.ylabel(ylab)
    plt.grid(False)
    if legend is not None:
        plt.legend(legend)
        # plt.title(savefilename)
    if xlim is not None:
        plt.xlim(xlim)
    if ylim is not None:
        plt.ylim(ylim)
    print(outdir+savefilename+'.pdf')
    if plt.gcf().get_axes()[0].get_title() == '':
        plt.title(identifier.replace('_', ' '), loc='left', color='black', fontsize=16)
    plt.savefig(outdir+savefilename+'.pdf', bbox_inches='tight')

# Simple scatter plots
print(f'w_maxlNew_{ratio}_FixPSD')
print(w_eco2s, maxle)
mass_ratio = [m2 / m1 if m2 < m1 else m1 / m2 for m1, m2 in zip(m_eco1s, m_eco2s)]
weco_ratio = [w2 / w1 if w2 < w1 else w1 / w2 for w1, w2 in zip(w_eco1s, w_eco2s)]
mw_ratio = [m2*w2 / (m1*w1) if m2*w2 < m1*w1 else m1*w1 / m2*w2 for w1, w2, m1, m2 in zip (w_eco1s, w_eco2s, m_eco1s, m_eco2s)]
scatterplot(w_eco2s, maxle, r'$\omega$', r'$\log \mathcal{L_\mathrm{max}}$', f'w_maxlNew_{ratio}_FixPSD')
plt.close()

scatterplot(w_eco2s, np.exp(np.array(logbe)-max_BF), r'$\omega$', r'$\mathrm{BF} / \mathrm{BF}_\mathrm{max}$', 'w_BFratio_FixPSD')
plt.close()

if args.strain in ["unequal", "everything"]:
    scatterplot(w_eco1s, logbe, r'$\omega_1$', r'$\log \mathrm{BF}$', 'w1_vs_logB')
    plt.close()
    scatterplot(w_eco2s, logbe, r'$\omega_2$', r'$\log \mathrm{BF}$', 'w2_vs_logB')
    plt.close()
    plt.title(r'$\omega_1$ v.s. $\log$ BF and $\log \mathcal{L_\mathrm{max}}$')
    plt.scatter(w_eco1s, logbe)
    scatterplot(w_eco1s, maxle, r'$\omega_1$', r'$\log \mathcal{L_\mathrm{max}}$', 'w1_logB_logL_NewFixPSD', legend=[r'$\log$ BF', r'$\log \mathcal{L_\mathrm{max}}$'])
    plt.close()
    plt.title(r'$\omega_2$ v.s. $\log$ BF and $\log \mathcal{L_\mathrm{max}}$')
    plt.scatter(w_eco2s, logbe)
    scatterplot(w_eco2s, maxle, r'$\omega_2$', r'$\log \mathcal{L_\mathrm{max}}$', 'w2_logB_logL_NewFixPSD', legend=[r'$\log$ BF', r'$\log \mathcal{L_\mathrm{max}}$'])
    plt.close()
else:
    plt.title(r'$\omega$ v.s. $\log$ BF and $\log \mathcal{L_\mathrm{max}}$')
    plt.scatter(w_eco2s, logbe)
    scatterplot(w_eco2s, maxle, r'$\omega$', r'$\log \mathcal{L_\mathrm{max}}$', 'w_logB_logL_NewFixPSD', legend=[r'$\log$ BF', r'$\log \mathcal{L_\mathrm{max}}$'])
    plt.close()
    scatterplot(w_eco2s, logbe, r'$\omega$', r'$\log \mathrm{BF}$', 'w_vs_logB')
    plt.close()

scatterplot(weco_ratio, logbe, r'min($\omega_2 / \omega_1$, $\omega_1 / \omega_2$)', r'$\log \mathrm{BF}$', 'wecoratio_vs_logB')
plt.close()

scatterplot(mass_ratio, logbe, r'$q$', r'$\log \mathrm{BF}$', 'massratio_vs_logB')
plt.close()

scatterplot(mw_ratio, logbe, r'min($(m_2\omega_2) / (m_1\omega_1)$, $(m_1\omega_1) / (m_2\omega_2$))', r'$\log \mathrm{BF}$', 'mwratio_vs_logB')
plt.close()

scatterplot(weco_ratio, mass_ratio, r'min($\omega_2 / \omega_1$, $\omega_1 / \omega_2$)', r'$q$', 'wecoratio_vs_massratio')
plt.close()

scatterplot(w_eco2s, m_eco2s, r'$\omega$', r'$M_\mathrm{model, 2}$', 'Mmodel_wecoFixPSD')
plt.close()

plt.title(identifier.replace('_', ' ') + '     ' + r'$\omega_\mathrm{eff}$ vs $a_f$' , loc='left', color='black', fontsize=16)
scatterplot(weff_ecos, final_spin_ecos, r'$\omega_\mathrm{eff}$', r'$a_f$', 'weff_af')
plt.close()

scatterplot(mass_ratio, logbe, r'$q$', r'$\log \mathrm{BF}$', 'q_vs_logB')
plt.close()

# Scatter plots with colour bar
plt.scatter(w_eco2s, maxle, c=final_spin_ecos,
            vmin=0.71, vmax=0.83, cmap='Blues', edgecolors="grey", linewidth=1)
cbar = plt.colorbar()
plt.xlabel(r'$\omega$')
plt.ylabel(r'$\log \mathcal{L_\mathrm{max}}$')
cbar.set_label(r'$a_f$', rotation=270, ha='right')
plt.grid(False)
plt.savefig(outdir+'w_vs_LogLMax_proca_color_afNewFixPSD.pdf', bbox_inches='tight')
plt.close()

plt.scatter(final_spin_ecos, maxle, c=w_eco2s,
            vmin=0.87, vmax=0.93, cmap='Blues', edgecolors="grey", linewidth=1)
cbar = plt.colorbar()
plt.xlabel(r'$a_f$')
plt.ylabel(r'$\log \mathcal{L_\mathrm{max}}$')
plt.title('Proca stars', loc='left', color='black', fontsize=20)
cbar.set_label(r'$\omega$', rotation=270, ha='right')
plt.grid(False)
plt.savefig(outdir+'af_proca_maxlNewFixPSD.pdf', bbox_inches='tight')
plt.close()

clb, cub = min(mass_ratio), max(mass_ratio)
af = np.array(final_spin_ecos)
ylb, yub = np.min(af[af > 0.6]) - 0.01, np.max(af) + 0.01
plt.scatter(w_eco2s, final_spin_ecos, c=mass_ratio,
            vmin=clb, vmax=cub, cmap='YlGnBu', edgecolors="grey", linewidth=0.5)
cbar = plt.colorbar()
plt.xlabel(r'$\omega$')
plt.ylabel(r'$a_f$')
plt.xlim(0.845, 0.935)
plt.ylim(ylb, yub)
cbar.set_label(r'$q$', rotation=0, ha='left')
plt.title(identifier.replace('_', ' '), loc='left', fontsize=20)
plt.grid(False)
plt.savefig(outdir+'w_vs_af_cmapq.pdf', bbox_inches='tight')
plt.savefig(outdir+'w_vs_af_cmapq.png', bbox_inches='tight')
plt.close()

# Histograms
plt.hist(mu_B*1e13, density=True, alpha=0.8, bins=30,
         color='C0', histtype='step')
plt.grid(False)
plt.xlabel(r'$\mu_V\;[\times 10^{-13} \mathrm{eV}]$')
plt.ylabel('PDF')
plt.title(identifier.replace('_', ' '), loc='left', color='black', fontsize=20)
plt.savefig(outdir+'boson_mass_30binsNewFixPSD.pdf', bbox_inches='tight')
plt.close()

plt.hist(iotae, density=True, alpha=0.8, bins=30,
         color='C0', histtype='step')
plt.xlabel(r'$\theta_{JN}$')
plt.ylabel('PDF')
plt.grid(False)
plt.savefig(outdir+'iota_histNewFixPSD.pdf', bbox_inches='tight')
plt.close()

lb, ub = np.min([omegae1, omegae2]), np.max([omegae1, omegae2])
plt.hist(omegae1, density=True, range=(lb, ub), alpha=0.8, bins=30,
         color='C0', histtype='step')
plt.hist(omegae2, density=True, range=(lb, ub), alpha=0.8, bins=30,
         color='C1', histtype='step')
plt.grid(False)
plt.xlabel(r'$\omega / \mu_V$')
plt.ylabel('PDF')
plt.legend([r'$\omega_1$', r'$\omega_2$'])
plt.xlim(0.85, 0.95)
plt.savefig(outdir+'w_hist_30FixPSD.pdf', bbox_inches='tight')
plt.close()

plt.hist(m_eco2s, density=True, alpha=0.35, color='C0')
plt.hist(Mproca2, density=True, alpha=0.35, color='C1')
plt.legend([r'$M_\mathrm{model}$', r'$M_\mathrm{total}$ (Proca)'])
plt.savefig(outdir+'Mmodel_FixPSD.pdf', bbox_inches='tight')
plt.close()

lb, ub = np.min([weffe, weffexe]), np.max([weffe, weffexe])
plt.hist(weffe, bins=35, range=(lb, ub), density=True, alpha=0.7, color='C0', histtype='step')
plt.hist(weffexe, bins=35, range=(lb, ub), density=True, alpha=0.7, color='C1', histtype='step')
plt.title(identifier.replace('_', ' ') + r'     $\omega_\mathrm{eff}$ distribution', loc='left', color='black', fontsize=16)
plt.legend(['Model mass', 'PE masses'])
plt.savefig(outdir+'weff_hist.pdf', bbox_inches='tight')
plt.close()

# 2D Histograms
corner.hist2d(de*np.sin(iotae)*np.cos(phie), de*np.sin(iotae)*np.sin(phie),
              color='C0', plot_datapoints=False, plot_density=False,
              smooth=True, levels=[0.9], fill_contours=False, bins=50)
plt.ylabel(r'$d_L\sin(\theta_{JN})\sin(\varphi)$')
plt.xlabel(r'$d_L\sin(\theta_{JN})\cos(\varphi)$')
plt.grid(False)
plt.savefig(outdir+'Orientation_ProjectedFixPSD.pdf', bbox_inches='tight')
plt.close()

corner.hist2d(np.array(Mproca1), np.array(Mproca2),
              color='C0', plot_datapoints=False, plot_density=False,
              smooth=True, levels=[0.9], fill_contours=False, bins=50)
plt.xlabel(r'$M_\mathrm{Proca,1}$')
plt.ylabel(r'$M_\mathrm{Proca,2}$')
plt.grid(False)
plt.savefig(outdir+f'TwoProcaMasses_{ratio}.pdf', bbox_inches='tight')
plt.close()

indices = np_afe != 0
np_xe_ = np_xe[indices]
np_afe_ = np_afe[indices]
np_logle_ = np_logle[indices]

corner.hist2d(np_xe_, np_afe_, color='grey',
              plot_datapoints=False, plot_density=False, smooth=True,
              levels=[0.9], fill_contours=False, bins=50)
le_max, le_min = np.max(np_logle_)+5, np.min(np_logle_)-5
plt.scatter(np_xe_, np_afe_, c=np_logle_, vmin=le_min, vmax=le_max, alpha=0.4, cmap='RdPu', label='Head-on Proca Stars')
cbar = plt.colorbar(pad=0.01)
plt.axhline(y=0.67, c='C0')
xmin, xmax = plt.gca().get_xlim()
plt.gca().set_xlim(xmin-3, xmax+3)
plt.gca().set_ylim(min(np_afe_)-0.05, max(np_afe_)+0.05)
plt.annotate(r'Proca stars', xy=((xmax-xmin+6)*0.1+xmin, 0.67), color='black', verticalalignment='top')
plt.xlabel(r'$(1+z)\;M_f\;[M_\odot]$')
plt.ylabel(r'$a_f$')
plt.legend(handlelength=2, linewidth=3)
cbar.set_label(r'$\log\mathcal{L_\mathrm{max}}$', labelpad=2)
plt.grid(False)
plt.savefig(outdir+f'MFaf_det_LogB_Fixed_Spin_Weighted_{ratio}.pdf', bbox_inches='tight')
plt.close()


# Direct copy from Bilby (bilby/core/result.py), with some modifications.
def plot_corner(result_dict, parameters=None, titles=True, filename=None, dpi=300, **kwargs):

    import os
    from pandas import DataFrame as df

    posterior = df.from_dict(result_dict['posterior'])
    default_param = ['luminosity_distance', incline, 'ra', 'dec']
    default_labels = [r'$d_L$ [Mpc]', r'$\theta_{JN}$ [rad]', 'RA [rad]', 'DEC [rad]']

    # bilby default corner kwargs. Overwritten by anything passed to kwargs
    defaults_kwargs = dict(
        bins=50, smooth=0.9, label_kwargs=dict(fontsize=16),
        title_kwargs=dict(fontsize=16), color='#0072C1',
        truth_color='tab:orange', quantiles=[0.16, 0.84],
        levels=(1 - np.exp(-0.5), 1 - np.exp(-2), 1 - np.exp(-9 / 2.)),
        plot_density=False, plot_datapoints=True, fill_contours=True,
        max_n_ticks=3, hist_kwargs=dict(density=True))

    defaults_kwargs.update(kwargs)
    kwargs = defaults_kwargs

    plot_parameter_keys = list(parameters) if parameters is not None else default_param

    # Get latex formatted strings for the plot labels
    if isinstance(parameters, dict):
        kwargs['labels'] = [parameters[key] for key in plot_parameter_keys]
    else:
        kwargs['labels'] = kwargs.get('labels', default_labels)

    # Unless already set, set the range to include all samples
    # This prevents ValueErrors being raised for parameters with no range
    kwargs['range'] = [1] * len(plot_parameter_keys)

    # Create the data array to plot and pass everything to corner
    xs = posterior[plot_parameter_keys]
    fig = corner.corner(xs, **kwargs)
    axes = fig.get_axes()

    # Extract the directory name from the indir
    basename = os.path.basename(result_dict['indir'][:-1])
    plot_title = 'Joint Posterior of {} (from {})'.format(result_dict['identifier'], basename)
    plt.rc('text.latex', preamble=r'\usepackage{underscore}')
    plt.suptitle(plot_title, x=0.4, y=0.95, ha='left', fontsize=27)

    map_txt = 'Max. a Posteriori: {:.2f} ({})\n'.format(result_dict['max_MaP']['value'], result_dict['max_MaP']['simulation'])
    logL_txt = 'Max. ' + r'$\log\mathcal{L_\mathrm{max}}$' + ': {:.2f} ({})\n'.format(result_dict['max_logL']['value'], result_dict['max_logL']['simulation'])
    maxBF_txt = 'Max. ' + r'$\log\mathrm{BF_\mathrm{max}}$' + ': {:.2f} ({})\n'.format(result_dict['max_logBF']['value'], result_dict['max_logBF']['simulation'])
    bf_txt = 'Avg. ' + r'$\log\mathrm{BF_\mathrm{max}}$' + ': {:.2f}\n'.format(result_dict['avg_logBF'])
    z_Q1_range, z_Q3_range = np.diff(list(result_dict['redshift'].values()))
    z_txt = r"Redshift: ${{{:.2f}}}_{{-{:.2f}}}^{{+{:.2f}}}$".format(
        result_dict['redshift']['p50'], z_Q3_range, z_Q1_range)
    mu_Q1_range, mu_Q3_range = np.diff(list(result_dict['boson_mass'].values()))
    mu_txt = r"Boson mass: ${{{:.2f}}}_{{-{:.2f}}}^{{+{:.2f}}} \times 10^{{-13}}$ eV".format(
        result_dict['boson_mass']['p50']*1e13, mu_Q1_range*1e13, mu_Q3_range*1e13)
    info_txt = map_txt + logL_txt + maxBF_txt + bf_txt + z_txt +'\n' + mu_txt

    fig.text(0.65, 0.85, info_txt, fontsize=20, transform=fig.transFigure, ha='left', va='top')

    # Add the titles on each subplots
    if titles and kwargs.get('quantiles', None) is not None:
        for i, par in enumerate(plot_parameter_keys):
            ax = axes[i + i * len(plot_parameter_keys)]
            if ax.title.get_text() == '':
                quantiles = np.array([kwargs['quantiles'][0], 0.5, kwargs['quantiles'][1]])
                Q1, Q2, Q3 = np.percentile(posterior[par], quantiles * 100)
                fmt = "{0:.3f}".format
                string_template = r"${{{0}}}_{{-{1}}}^{{+{2}}}$"
                quant_str = string_template.format(
                    fmt(Q2), fmt(Q2-Q1), fmt(Q3-Q2))
                ax.set_title(quant_str, **kwargs['title_kwargs'])

    if filename is None:
        filename = '{}/joint_posterior_{}_corner.png'.format(result_dict['outdir'], result_dict['event_name'])
        logging.debug('Saving corner plot to {}'.format(filename))
    fig.savefig(filename, dpi=dpi)
    plt.close(fig)

    return fig

parameters = {'luminosity_distance': r'$d_L$ [Mpc]',
              incline: r'$\theta_{JN}$ [rad]',
              'total_mass': r'$M_{tot}$ [$M_\odot$]',
              'mass_1': r'$m_1$ [$M_\odot$]',
              'mass_2': r'$m_2$ [$M_\odot$]',
              'ra': r'$\alpha$ [rad]',
              'dec': r'$\delta$ [rad]',
              phase_ang: r'$\phi$ [rad]'}

logging.info("Plotting joint posterior corner plot.")
plot_corner(result_dict, parameters)


proca_param = {'proca_mass_1': r'$M_\mathrm{model, 1}$ [$M_\odot$]',
               'proca_mass_2': r'$M_\mathrm{model, 2}$ [$M_\odot$]',
               'omega_1': r'$\omega_1$',
               'omega_2': r'$\omega_2$',
               'omega_eff': r'$\omega_\mathrm{eff}$',
               'final_spin': r'$a_f$'}

logging.info("Plotting Proca joint posterior corner plot.")
plot_corner(result_dict, proca_param, filename=outdir+'/proca_param_posterior_{}_corner.png'.format(event_name))

## Print DONE ##
print(     " ____     ___    _   _   _____ "     )
print(     "|  _ \   / _ \  | \ | | | ____|"     )
print(     "| | | | | | | | |  \| | |  _|  "     )
print(     "| |_| | | |_| | | |\  | | |___ "     )
print(     "|____/   \___/  |_| \_| |_____|"     )
